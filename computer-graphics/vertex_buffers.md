# Vertex Buffers
>"OpenGL is a state machine"
>* [Vertex Buffers in OpenGL](youtube.com/watch?v=0p9VxImr7Y0)


### Drawing a 2D trianglein OpenGL using vertex buffers

Lets keep things simple (and following the guide I found, linked above), to draw a 2D triangle.  The guide uses C++ as the programming language to interact with openGL
```py
glBegin(GL_TRIANGLES)
glVertex2f(-0.5, -0.5)
glVertex2f(0.0, 0.5)
glVertex2f(0.5,-0.5)
glEnd()
```
This would be a static triangle (meaning, the data for where the points are will not change), so it's quite a waste of processing power to have to go through this every time we render a frame.

In order to pass those points to a buffer, we shall change it into an array of floats, each pair of numbers representing a single point.

```cpp
int positions_array[6]  = {
    -0.5, -0.5,
    0.0, 0.5,
    0.5,-0.5
};
```
Then we set up the buffer
```cpp
//create a buffer, we specify how many buffers we want
buffer_id = glGenBuffers(1);

//bind that buffer as a ARRAY_BUFFER, saying "we're going to be working with this"
glBindBuffer(GL_ARRAY_BUFFER, buffer_id) ;

//feed the buffer some data, specifying the size of the array in bytes. 
glBufferData(GL_ARRAY_BUFFER, 6* sizeof(float) ,GL_STATIC_DRAW);
```
>We know it's 6 floats. And then give it a hint of how it will be used, GL_STATIC_DRAW tells it will be static and for drawing.

Finally we unbind the `GL_ARRAY_BUFFER` so it can be used again
```cpp
glBindBuffer(GL_ARRAY_BUFFER,0);
```