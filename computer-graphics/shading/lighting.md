>[Opengl tutorial: Basic shading](opengl-tutorial.org/beginners-tutorials/tutorial-8-basic-shading/)
# Shading / Lighting
> __Polygon mesh__ is a collection of vertices, edges and faces that defines the shape of a _polyhedral_ object. It consists of either triangles, quads or other convex polygons (n-gons), to simplify rendering.

_Shading_ is part of the __rasterization__ process where 3D models are drawn as 2D pixel images. Shading applies a lighting model, in conjunction with the geometric attributes of the 3D model, to determine how lighting should be represented at each fragment/pixel of the resulting image. 

## Types of shading
![flat vs. phong](images/flat-vs-phong-shading.jpg)

Here is an example of flat shading vs. phong shading, the models are the same in both examples (you might notice the slightly jagged edges along the smoooth sphere against the background), flat shading just shows the polygons clearly, while phong uses calculations to make the object look smooth in rasterization.

* ### Flat shading
    > It is inexpensive shading, where every surface is as flat as our model polygons would be. A single vertex is used to calculate the shading for the entire polygon.
* ### Gouraud shading
    >Gouraud shading generates a smooth lighting effect across the 3D model's surface
* ### Phong shading
    >Phong shading is a specific type of shading technique in 3D computer graphics that is useful for smoothing out multi-surface shapes and creating more sophisticated computer-modeled images.  It is also known as __Phong Interpolation__ or _normal-vector interpolation shading_
    >
    >It _interpolates_ surface normals across rasterized polygons and computes pixel colors based on the interpolated normals and a reflection model. 
    >
    >
    >![](images/phong_components2.png)
    >
    >>It is similar to _Gouraud shading_, as it is another type of interpolative shading that blends between vertex values and shade polygons. The key difference between the two is that Phong shading interpolates the _vertex normal_ values over the whole polygon before it calculates the shading. This contrasts with Gouraud shading which interpolates the already shaded vertex values over the whole polygon.
    
## Lighting aspects
![Revised phong lighting](images/Phong_components_revised.png)
* ### Ambient lighting
    >This is a light source that represents an omni-directional, fixed-intensity and fixed-color light source that effects all objects in the scene equally. It seemingly comes from everywhere without a fixed source. 
    >
    >In other words, it adds a __flat color__ across the everything
* ### Diffused lighting (matte)
    >This is essentially light on a _matte surface_ (not shiny), its intensity is not effected by camera location, only by the light source. Diffuse is also the color of the light.
* ### Specular lighting (shiny)
    >This is the reflective light, the shiny highlights that seem to move with the camera, because its intensity is effected by how close the direction of the light's reflection vertex comes to the camera's location. 


## Diffused Lighting
Lighting is important for the intensity/brightness of colors, and __diffused lighting__ is essentially lighting on a _matte_ surface (not shiny or reflective). Only the light source effects the intensity, not camera location.

When light hits an object, an important fraction of it is reflected in all directions. This is called the __diffuse component__.  

And in order to know at what angle the light hits the surface, we will need to get the __vertex normals__, which lets us know in which direction the surface is facing.
>![](images/diffuseWhite1.png)

The surface is illuminated differently according to the __angle__ at which the light arrives.

>![](images/diffuseAngle.png)

If the light is __perpendicular__ to the surface, it is concentrated on a small surface.  If it arrives at a grazing angle, the same quantity of light spreads on a greater surface (illuminating more area).

Grazing light will make the surface look darker.

>#### Color/Light and calculation
>The color value is therefor `color = lightColor * cosTheta` (the angle of the light, and it's color).
>
>#### Beware of negative angle
>If the light is coming from beind the shape (negative angle), then the color would become negative, which doesn't really make sense, so we will limit the `cosTheta` to 0, instead of allowing _negative_ angles.

### Diffusing on material color
You might be familiar with how the color of objects can _bounce_ with the light, giving color to nearby objects. This can be taken into account as well.
>![](images/diffuseRed.png)

```
color = MaterialDiffuseColor * LightColor * cosTheta
```

### Modeling the light (making it realistic)
Previous examples have given our scene a bit more depth by coloring model surfaces based on how much light can reach it. 

But we can increase the realism.

>#### Fading light with distance
>The `distance` is in the equation because light diminishes with distance, the further it is from the source.
>```
>color = MaterialDiffuseColor * LightColor * cosTheta / (distance*distance)
>```

>#### Light strength
>Not all light is equal, the strength decides how bright it is.
>```
>color = MaterialDiffuseColor * LightColor * LightPower * cosTheta / (distance*distance)
>```


## The Math for light intensity

![light vectors](images/light_math_vectors.png)

![lambert](images/lambert_math.png)

![Phong](images/phong_math.png)
>We use the _max_ to limit the outcome, we do not wont to go into negative values.
* `eye` is the position of the camera
* `m` = normal vector of the _material_ surface
* `p` = point on the screen we're looking at
* `s` = vector of the light _source_
* `v` = vector to the camera from `p` 
    >`eye - p`
* `h` =  _halfway vector_ between v and s
    > `1/2 * (s+v)`

### Putting it all together
> _I = I<sub>a</sub> &rho;<sub>a</sub> + I<sub>d</sub> &rho;<sub>d</sub> * lambert + I<sub>s</sub> &rho;<sub>s</sub> * phong<sup>f</sup>_
>* I = light color intensity
>* &rho; = Material color value
>* a = ambient color value
>* d = diffuse color value
>* s = specular color value
>* f = shininess level

Each aspect (the light, material and ambience) have 3 color values: _red_, _green_ and _blue_. Each needs to be calculated separately to get the final color value of the pixel.

## Definitions
* ### Lambert
    >__Lambertian reflectance__ is the property that defines an ideal "matte" or _diffusely_ reflecting (reflection of light/particles) surface. The apparent brightness of a lambertian surface to an observer is the same regardless of the observer's angle of view. 
    >
    >In computer graphics, lambertian reflection is often used as a model for diffuse reflection. This technique causes all closed polygons to reflect light equally in all directions when rendered. In effect, a small flat regiion rotated around its normal vector will not change the way it reflects light. However, the region will change the way it refleccts light if it is tilted away from its initial vector because the area is illuminated by a smaller fraction of the incident radiation/light.
    >
    >The reflection is calculated by taking the _dot product_ of the surface's normal vector, and normalized light-direction vector, pointing from the surface to the light source. This number is then multiplied by the color of the surface and the intensity of the light hitting the surface.
    >
    >![](images/lambert_calculation.svg)
    >* N is the surface's normal vector
    >* L is the light direction vector
    >
    >Lambertian reflection from polished surfaces are typically accompanied by a _specular reflection (gloss)_, where the surface luminance is highest when the observer is situated at the perfect reflection direction (i.e. where the direction of the reflected light is a reflection of the direction of the incident light in the surface), and falls off sharply.
    