# Shaders
You can't tell the shape or color of objects without shaders. Light is important to decide how _bright_ a color appears, and if the light itself has a color other than white, then the materials will be effected by that.

## Vertex Shader
> Calculates the vectors needed by the _fragment shader_

## Fragment Shader
> Calculates the colors of the pixels (fragments). The diffuse of the material and light (essentially their color), how shiny the surfaces are (specularity), how reflective they are (shinyness and eye coordinates come into play).
