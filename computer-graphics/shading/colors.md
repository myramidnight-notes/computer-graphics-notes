# Colors

>When we give _openGL_ a vertex, we can actually give each a color individually (a defined color effects all vertices that come after it's decleration). It gives a interesting effect of colors blending between each vertex.

### Color-buffer
>If we do not clear the __color-buffer__, then everything that was drawn in previous frame will stay on the screen, creating odd effects and smearing (when things move around).
>
>So clearing the screen between frames is important.
>```
>glClear(GL_COLOR_BUFFER_BIT);
>```

### Far and Near
>![](images/color-cube-far.png)
>
>This interesting effect happens when the program doesn't really take the distance of the vertices into account, so vertices that are further away might get __drawn over__ whatever was drawn before, regardless of how far or near to the view it should be.
>
>This does show us however that the program draws every vertex, regardless if we're supposed to be able to see that side or not. It simply gets over-drawn so we don't see it. 
>
>Note: This would probably make a program quite _heavy_ if there are aloooot of verticies in the scene (even the things off screen).

### Adding depth (Z-buffer)
This simply keeps track of the distance at which the vertices are located, so that they can be drawn in a correct order so that things further away aren't overlapping what's closer to our view.

This buffer allows the program to render 3D correctly, to take the `z` position of the vertices into account (since everything drawn are just flat images, it's just an illusion of 3D that we're trying to create)

>```c++
>glEnable(GL_DEPTH_TEST);
>```
>Without this enabled, 3D shapes will get a interesting inverted look.

We will of course need to remember to clear the z-buffer between frames as well. Else only moving objects get drawn. 
>```
>glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
>```

![](images/color-cube.png)