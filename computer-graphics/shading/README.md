# Shading
> Colors are effected by light, bounching on the surface. And for very basic calculations, we can just imagine we're in space, so there are no surfaces for the light to bounce around on.

* ### [Colors and Depth](colors.md)
* ### [Lighting](lighting.md)
    > Lighting, Shading and the math
* ### [Shading](shading.md)
    > Lambert

## Other node scribbles

> Dot product between two vectors is 0, which means they are perpendicular (samsíða) and for the lighting, it would simply mean that the light vector didn't hit the surface, there for it doesn't get lit. 
>
>We don't let the value go into negative.

> cosine of two vectors (their angle) is zero if they are at 90 degrees, we don't want negative, and not absolute values.
> The length of the vector can indicate the strenght of the light.

## Unused / Useless variables
If we have a unused or useless variable in the vertex shader, they will be deleted for optimization. 

So when we go into our program and try to bind the variables to use them, they won't exist. Which will cause errors if they were previously bound but no longer used, because the variable will no longer exist

This also includes if something is overwritten, causing all the previously used variables to have no effect, they get deleted.

>So it's not enough to define a variable, need to use it!