# Clipping
We will want to find the point on the line that crosses the window border, and cut the line at that point, making it shorter to only include the portion that we actually see.

Best to have a cheap way to exclude things from being sent into the pipeline if they can't be seen on the screen (pointless to _render_)

To make a program lighter for the processor, it would be best if we could calculate what objects/vertices should be included (we don't want to waste processing power to _render_ something that is outside of our view)

It is _cheaper_ to figure all this out by calculating vertices before it goes further into the pipeline.

### Catching objects/verticies that are within view
If we were rendering lines, we could calculate if either of their endpoints are within the window, but then we might run into the issue of lines which has endpoints outside of the window but the line crosses our view.
* Can check if the `x` of both points is outside of the window, and same for the `y`, so the line gets neither accept (confirmed point within window) or reject (confirmed to be entierly out of our view), then we can catch lines that have endpoints outside of our view entierly. 

```python
# psyeudo code, checking a single point.
if (outside left)
elif (outside right)
elif (outside top)
elif (outside bottom)
```
if endpoint is outside of both sides, then we would clip the line from one side, then go back through the loop to clip it from the other side, then the endpoint has been readjusted to be within the window. This is cheaper than having to calculate clipping from both sides at once.

This way of clipping one side at a time, can let us decide if lines that might be partially within view but both endpoints outside of the window.

### Line clipping
It is easy to discard or include lines that are confirmed to be fully within or outside of our window. It are the partially seen lines that we need to think about.

Lines that have one endpoint confirmed within the view can have a formula that recalculates the other endpoint to be within the window.

## Algorithms

### Cohen-Sutherland algorithm (line clipping)
>Checks all the lines/points in multiple passes, only trimming based on one edge at a time (becaues lines can have a endpoint that is outside both edges)
>
>This works for all lines that cross the window in any way, even if both endpoints are outside of the window.
>
>It depends on the implementation of the algorithm which side gets clipped first, with each round the line will be trimmed until both endpoints are within view.
![](images/line-clipping.png)
* [Line clipping (wikipedia)](https://en.wikipedia.org/wiki/Line_clipping)


### Sutherland Hodgman algorithm
>Algorithm that takes a input list of vertices of the subject polygon, and clips it one side at a time. Any number of sides, but we would usually be working with the four sides of the window.
>
>![Sutherland-hodgman clipping](images/Sutherland-Hodgman_clipping_sample.svg)

