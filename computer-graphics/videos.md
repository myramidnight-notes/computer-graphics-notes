
## Lectures
* [Window to Viewport mapping](https://youtu.be/U9cY0YxpAVs) (older lecture: [from 2019](https://youtu.be/gW_U9WjYciY))
* [Transformations in 2D](https://www.youtube.com/watch?v=0lQY_MMoxmU)
* [Transformations and Matrices](https://youtu.be/hk6AcHrTEQY)
    * [geometrical transformations](https://www.youtube.com/watch?v=MrxK0W3-ZiM)
    * [__solutions__: calculating transformation matrices](https://www.youtube.com/watch?v=i7pFOJVGn1I)
### 3D
* [View: Camera position and orientation](https://youtu.be/2s6SO7ElsiA)
    * [position and orientation of the eye/camera in 3D](https://www.youtube.com/watch?v=eQeMOc0xq2g)
    * [__solutions__: calculating camera transformations (view and projection)](https://www.youtube.com/watch?v=oICzySFODbg)
* [Camera motion, first 3D program](https://youtu.be/yyKa8Hqklpk)
* [Projection: camera "lens"](https://youtu.be/3GxkkYd27Oo)
    * [Perspective projection](https://www.youtube.com/watch?v=kYn2piwLrMw)
* [Clipping](https://youtu.be/EeZad4Tr_dk)
    * [__solutions__: cohen-sutherland clipping](https://www.youtube.com/watch?v=1V1Ju6koW2M)
* [Lighting](https://youtu.be/fm-vj6EEpS0)
    * [picture of whiteboard after lecture](images/Lighting_Whiteboard.jpg)
    * [lighting calculations](https://www.youtube.com/watch?v=B0otETNacDo)
    * [__solution__: lighting calculations](https://www.youtube.com/watch?v=Oqx_mHag9Rs)
* [3D meshes](https://youtu.be/lCfcDAEzprU)
    * [Meshes and model files](https://www.youtube.com/watch?v=lpPwpnfML00)
    * [live lecture recording](https://youtu.be/dSY0u0XVtkc)
* [collisions and camera setup and control in 3D](https://youtu.be/nME5Kol431E)
* [Rasterization](https://youtu.be/dNM6QYti5GM)
    * [__solutions__: rasterization](https://www.youtube.com/watch?v=mCvvbJX_y2g)
* [Texture mapping](https://youtu.be/VBpax3vzOa0)
* [Motion and Bezier curves ](https://youtu.be/KhyTMQpACjI)
    * [Motion and Bezier curves (on digital whiteboard)](https://www.youtube.com/watch?v=2zJ0E3Sqdto)
* [3D camera motion](https://www.youtube.com/watch?v=yyKa8Hqklpk)  
* [Pipeline overview and recap](https://youtu.be/WhxmRc8aRvA)
    * [OpenGL pipeline overview](https://www.youtube.com/watch?v=qfEg2o40kB4)
    * [Overview: first lecture introduction](https://youtu.be/1pTE3KM_TEk)
## Programming sessions
>They are not always in the same programing language, but it's the same idea, and it's still OpenGL
* [Lab exersize: first 2D](https://youtu.be/U5uzVjNeUCk)
* [Transformations and code refactoring (2D)](https://youtu.be/G7zjZ_ZUjgk)

### 3D
* [lab exercise: first 3D](https://youtu.be/zDIUdITmD0c)
* [camera in 3D](https://youtu.be/0bxr756xQ-Y)
* [Lighting and Shaders](https://youtu.be/6EKUzN9oFoc)
* [Programming session: Textures (in java)](https://www.youtube.com/watch?v=STAl-QnUosk) 
* [Texture mapping in program (in python)](https://youtu.be/9Z1-XNpr3hs)
* [Programming session: Fog](https://www.youtube.com/watch?v=jVshgMC9cMU)
* Special lab exercise: loading and rendering mesh models
    * [Vertex buffer objects](https://youtu.be/o8plFPc6Jvo)
    * [obj.files, Blender and cylindrical texture map on sphere](https://youtu.be/Tt-pfenrst4)

## Other videos
* [PointLights, DirLights, Multiple lights](https://www.youtube.com/watch?v=RSt4ZLEzs_A)

## Handy
* [The beauty of Bézier curves](https://www.youtube.com/watch?v=aVwxzDHniEw)
* [Fast inverse square root - A quake3 algorithm](https://www.youtube.com/watch?v=p8u_k2LIZyo)
    ```cpp
    // Old way: 
    // float y = 1 / sqrt(x);

    float Q_rsqrt(float number)
    {
        //Some 32 bit numbers
        long i;
        float x2, y;
        const float threehalfs = 1.5F;

        x2 = number * 0.5F;
        y  = number;
        i  = *(long*) &y;                       // evil floating point bit hack
        i  = 0x5f3759df - (i >> 1);             // WTF
        y  = *(float*) &i;
        y = y * (threehalfs - (x2 * y * y ));   // 1st iteration (or Newton Iteration)
    // y = y * (threehalfs - (x2 * y * y ));   // 2nd iteration, can be removed
    }
    ```
    >Multiplication is fast, but square root is slow. And we need it if we're normalizing vectors. The _Q_rsqrt_ is 3 times as fast.