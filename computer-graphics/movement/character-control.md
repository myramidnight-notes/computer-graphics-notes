* __Clamping__: stopping the camera from moving too far in a given direction (such as moving up until we're flipping over backwards)


* [Text based 1st person game](https://www.youtube.com/watch?v=xW8skO7MFYw)
* [Setting up 1st person camera](https://thepentamollisproject.blogspot.com/2018/02/setting-up-first-person-camera-in.html)
## Mouse view
```python
import pygame
class MouseLook:
    def __init__(self):
        self.sensitivity = 1000 #mouse sensitivity, speed

    
    def update(self, delta_time):
        mouse_pos = pygame.mouse.get_pos
        mouse_x = mouse_pos.x * self.sensitivity * delta_time


```