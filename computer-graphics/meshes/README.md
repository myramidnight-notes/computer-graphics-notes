# 3D Meshes



![](images/Mesh_overview.svg)
* ### Vertices
    >The points that make up the shape
* ### Edges 
    >Edges are just the lines
* ### Faces
    > Each of the triangles (meshes are generally made up of triangle faces). 
* ### Polygons
    >"Polygon is a plane figure that is described as a finite number of straight line segments connected to form a closed polygonal chain". __They are a flat shape.__
    >
    >`<num of vertices>:<order of vertices> <order of normals>` 
    >
    >Good to have front face order clockwise.
* ### Surfaces
    >Collection of polygons that make a single surface. A single model can be split into multiple surfaces, not always bound by which side they are. 

## Normals for a mesh
>If it's not clear yet, normals tell the program in what direction the vertex is facing, which eventually will be translated to identify the surface (the outward faces?).

The normals for a mesh object is a list of vertices equal to the number of vertices being sent in and same order. But since each polygon is a group of these vertices, all the normals for a single polygon are usually all the same.
> I am not sure if a single polygon can have multiple normals, since they can be specified indiviually. Maybe it's done for the illusion of non-flat polygon for the shader?

The polygon list could specify a single normal that applies to every vertex of that polygon, or they could specify the normal for each vertex.

## In context of OpenGL
1. We have the list of vertices, and list of normals
1. We give the shader these two lists
    ```py
    def set_vertices(self, shader):
        shader.set_position_attribute(self.position_array)
        shader.set_normal_attribute(self.normal_array)
    ```
1. Then we tell openGL to draw the polygons (telling it the indexes of the range of vertices from the list of vertices).
    ```py
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4)
    glDrawArrays(GL_TRIANGLE_FAN, 4, 4)
    ...
    ```
    >It is using the array from the shader that we sent in, that is why we're not specifying the array.