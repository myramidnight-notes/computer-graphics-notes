
* [Anti-aliasing: which one do you need?](https://laptrinhx.com/anti-aliasing-which-one-do-you-need-1157805669/)
* [What is Anti Aliasing (AA) as fast as possible](https://www.youtube.com/watch?v=hqi0114mwtY)
    >Describing it as being "fake resolution", making things appear to be in higher resolution without having to actually have more pixels.
    * [Different types of anti-aliasing as fast as possible (video)](https://www.youtube.com/watch?v=NDo5TKr6pyc)
* [Anti aliasing: which one do you need?](https://laptrinhx.com/anti-aliasing-which-one-do-you-need-1157805669/)

## Aliasing and Anti-Aliasing (AA)
> The aliasing effect is the appearance of sharp jagged edges (also called jaggies) in a rasterized image (an image rendered using pixels). The problem of jagged edges technically occurs due to distorion of the image when scan conversion is done with saimpling at a low frequency, which is also known as undersampling.

>__Antialiasing__ makes these curved or slanted lines smooth again by adding a slight discoloration to the edges of the line or object, causing the jagged edges to blur and melt together. If the image is zoomed out a bit, the human eye can no longer notice the slight discoloration that antialiasing creates.



![](images/antialiasing3.png)
![](images/antialiasing.png)
![](images/antialiasing-line.gif)


![](images/antialiasing-small-example.png)
> Here we can see how the a model of a plane gets rasterized, and how both the number of samples taken and the number of pixels can have a huge effect on how the plane is rendered. The dots represent the number and location of samples taken. Aliazing just takes a single sample, while antialiazing takes four.
>* __A__ shows aliazing on a 4x4 pixel grid. Resulting in __E__.
>    >We see how the plane wasn't caught by any of the samples, so this case doesn't render the plane at all.
>* __B__ shows antialiazing on a 4x4 pixel grid. Resulting in __F__.
>    >With the antializing, two of the pixels caught the plane, and the number of samples that caught them within a single pixel decides how much of each color to blend. The color is decided by the percentage of samples caught what color.
>* __C__ shows aliazing on a 8x8 pixel grid. Resulting in __G__.
>   >With the greater numberr of pixels, we have a higher chance of catching the plane, but with aliazing, we can hardly see the shape of the plane (it's better than nothing at all from example E).
>* __D__ shows antialiazing on a 8x8 pixel grid. Resulting in __H__.
>   > Finally we have a blurry looking plane, we can compare G and H and see how "that could be a plane". The shape is clearer than in case G.

### Multi-Sample Antialiasing (MSAA)
There are a few types of antialiasing, and what we've been talking about would be called Multi-sample anti-aliasing.
![](images/multi-sampling.jpg)
