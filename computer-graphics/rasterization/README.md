* [3D basic rendering: rasterization stage](https://www.scratchapixel.com/lessons/3d-basic-rendering/rasterization-practical-implementation/rasterization-stage)
* [Rasterization: a practical implementation](https://www.scratchapixel.com/lessons/3d-basic-rendering/rasterization-practical-implementation/perspective-correct-interpolation-vertex-attributes)
# Rasterization
_Rasterization_ is the process by which a primitive is converted to a two-dimensional image. Each point of this image contains such information as color and depth. Thus, rasterizing a primative consists of two parts
(OpenGL specifications).
1. First part is to determine which squares of an integer grid in window coordinates are occupied by the primative.
1. Second part is assigning a color and depth value to each such square. 

### Pixelated
>We have to keep in mind that each pixel of the screen can only be one color or another, having more pixels simply allow us to make things seem smoother. So if we are rasterizing to a very small pixel frame, things might start looking unclear ("pixelated")
>
>![](images/rasterization-triangle.png)
>
> by testing if pixels in a image overlap the triangle, we can draw an image of that triangle. This is the principle of rasterization algorithm


![](images/point_contained_cross_product.gif)
> P is contained in the triangle if the edge function returns a positive number for the three indicated pairs of vectors.

## Perspective interpolation
![](images/naive-perspective-depth.png)
* Naive interpolation
    >The difference is subtle, but in the left image you can see how all the colors fill roughly the same area. This is due to the fact that colors in this case are interpolated as if the triangle is in 2D space, making it seem as if it is parallel to the screen (flat).
    >
    >But viewing the z-buffer, we can see that it is not paralel to the screen, but instead oriented at an angle.
* Perspective correct interpolation
    >If we use perspective correct interpolation, then the vertex attribute values are dividded by the z-coordinate of the vertex they are associated to. 
    >
    >In respect to the z-buffer, the corner colored green is closer to the camera than the other points, this causes this part of the triangle to fill up a larger part of the screen which is visible in the middle image.
* Z-buffer
    >Depth will make an object seem brighter the closer it is to the screen (distance fading).

The difference between correct and incorrect perspective interpolation is even more visible when applied to texturing.

## [Aliasing and Antialiasing](antialiasing.md)
> The aliasing effect is the appearance of sharp jagged edges in a rasterized image (an image rendered using pixels). The problem of jagged edges technically occurs due to distorion of the image when scan conversion is done with saimpling at a low frequency, which is also known as undersampling.

>__Antialiasing__ makes these curved or slanted lines smooth again by adding a slight discoloration to the edges of the line or object, causing the jagged edges to blur and melt together. If the image is zoomed out a bit, the human eye can no longer notice the slight discoloration that antialiasing creates.