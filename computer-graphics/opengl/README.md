* [video lecture](https://www.youtube.com/watch?v=Px6jDvNLJOM)
# Introduction to OpenGL
We can view the OpenGL pipeline as a state-machine
  * `glu` is a library that simplifies the GL functions for the programmer 

```
gluOrtho2D()
glViewport()
```
These functions define the window (in window-to-viewport).

We create a list of vertices with `glbegin` and `glend`
When we send in a list of verticies to this pipeline, it will use the matrix defined by the window-to-viewport state.

## Pipeline/ States
<!-- ![pipeline](images/opengl-pipeline.png) -->

1. List of Vertices
    > `glbegin()` and `glend()` tells the pipeline to listen to the inputted vertices.
    >
1. Vertex Shader
1. Clipping
1. Window-to-viewport
1. Frame buffer
1. Dept buffer
1. ...

# Index
* ### [OpenGL shaders](shaders.md)
* ### [Drawing basic shapes](drawing_shapes.md)
* ### [Basic initiate pygame](basic_initiate.md)

