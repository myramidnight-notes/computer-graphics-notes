# Basic initiate pygame
Let's set up the basics, get pygame to open a window and draw a shape, and notice keyboard input.

## Setup
We want to keep things organized, so let's encapsulate each section of the code
* `init_game()`
    > Initiate pygame
* `update()`
    > For things that change between frames
* `display()`
    > For displaying the frame on screen
* `game_loop()`
    > The code for running the game, the game logic so to speak

```py
# something to make it easier to test our code
if __name__ == "__main__":
    init_game()
    while True:
        loop()
```

## Imports
* `pygame` and `OpenGL` will of course need to be installed
```py
import pygame
from pygame.locals import *

from OpenGL.GL import *
from OpenGL.GLU import *
```

## Initiate the game
```py
def init_game():
    pygame.init()
    pygame.display.set_mode((800,600), DOUBLEBUF|OPENGL)
    #glClearColor(1.0, 0.0, 0.0, 0.5)
```
* Here we are initializing `pygame` with `init()`
* the `pygame.display.set_mode()` allows us to set the essentials
    * view port will be `(800,600)`
    * and we'll be using OpenGL with double buffering
        > Double buffering means that there are two images at any given time, one that we can see and one that we can transform as we see fit.We get to see the actual change caused by the transformations when the two buffers swap.
* The commented out line `glClearColor()` lets us set the background, it uses `RGBA` with ranges 0-1 (essentially percentage, with 1 being 100%)


## Draw shapes

Here is a code snipped, a function that draws a triangle. They would go into the `display()` section of our code essentially.

```py
def rez_tri():
    # draw a triangle
    glBegin(GL_TRIANGLES)
    # add this line if nothing appears or want to change the color
    glColor3f(0.0, 1.0, 1.0)
    glVertex2f(100, 100)
    glVertex2f(100, 200)
    glVertex2f(150, 100)
    glEnd()
```

* `glBegin()` is a function that indicates that we are going to start defining vertrices of a primative shape in the code. When we're done listing the vertices, then we use `glEnd()` to tell it to stop listening for more verticies.
* `GL_TRIANGLES` is one of many macros that tells the program what we're trying to do with the vertices. In this case it's triangles, so group of 3 vertices we list will be creating a triangle on the screen.
* `glVertex2f()` means that we're specifying a vertex using `floats` in a 2D space, `glVertex2i` would for example be the same just with integers.  

## Update
```py
def update():
    pass
```
> Here we would define anything would change between frames

## Display the frame
```py
def display():
    # add the color from glClearColor
    glClear(GL_COLOR_BUFFER_BIT)

    # display details
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    glViewport(0, 0, 800, 600)
    gluOrtho2D(0, 800, 0, 600)
    
    rez_tri()

    # the flip should be at the end of display()
    pygame.display.flip()
```


## Game Loop
```py
def game_Loop():
    # way to exit the game
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            quit()
        elif event.type == pygame.KEYDOWN:
            if event.key == K_ESCAPE:
                pygame.quit()
                quit()

    update()
    display()
```
> Any game logic we have would come in there, above the `update()` and `display()`, such as a easy way to exit the game. 
>* Here we set it so that pressing `ESC` will quit the game