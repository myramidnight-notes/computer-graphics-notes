* [video: transformations in 2D program and code refactoring](https://www.youtube.com/watch?v=G7zjZ_ZUjgk)
## Primitives and Shapes
`glbegin(GL_TRIANGLES)` will be a list of verticies for triangles, each group of 3 verticies becoming a triangle to be drawn. `GL_LINES` would create lines from each pair of vertices.

We can add `glColor` in with the vertices, which would give each vertex a color, creating a gradient of color for the shape.

There is also `GL_TRIANGLE_STRIP` and `GL_TRIANGLE_FAN` (good for making circles, all shapes will use the first vertex, previous and current)
![](images/OpenGL-primitives.png)
![](images/triangles.png)
> `QUADS` is not always implemented, but you can create quads with triangles anyways. Same goes for `POLYGON`, it can be created with `TRIANGLE_FAN`

