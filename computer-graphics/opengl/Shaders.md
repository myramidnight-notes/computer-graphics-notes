# Shaders (python)
The programming language that the OpenGL uses in their _vertex shader_ and _fragment shader_ seems to be based on __C__, but it's still it's own language, because it leaves out the things it doesn't need, and has it's own implemented structures, such as vectors, points and their own variable types.

So it doesn't really matter what programming language you pick to implement a OpenGL program, you just need to be able to get a OpenGL package for your language, and after that you can essentially follow any guide (even in other language) to get an idea of how things are done.

Its all about how you get your information to the shader it seems, at least when you get into the 3D.

## Structure of shader handler
>At least it's the basic structure of it done in python

1. open Vertex Shader
1. open Fragment Shader
1. setup the rendering program
1. create handles for the variables in the shaders
    >There are attribute and uniform locations (locations of the variable). Then you can start passing values to the shaders through these handles.
1. use the program  (`glUseProgram`) after setting the handles
1. define the methods to pass values from your code to the shaders.
    >You specify what type of value you are going to be passing along, such as `glUniformMatrix4fv` (matrix of floating point numbers for `vec4` vectors), `glUniform4f` (uniform variable containing 4 floating point numbers), `glUniform1i` would be a single integer uniform variable, and so on..

Here is the start of the Shaders file that uses `.vert` and `.frag` 
```py
class Shader3D:
    def __init__(self):
        vert_shader = glCreateShader(GL_VERTEX_SHADER)
        shader_file = open(sys.path[0] + "/shaders/simple3D.vert")
        glShaderSource(vert_shader,shader_file.read())
        shader_file.close()
        glCompileShader(vert_shader)
        result = glGetShaderiv(vert_shader, GL_COMPILE_STATUS)
        if (result != 1): # shader didn't compile
            print("Couldn't compile vertex shader\nShader compilation Log:\n" + str(glGetShaderInfoLog(vert_shader)))

        frag_shader = glCreateShader(GL_FRAGMENT_SHADER)
        shader_file = open(sys.path[0] + "/shaders/simple3D.frag")
        glShaderSource(frag_shader,shader_file.read())
        shader_file.close()
        glCompileShader(frag_shader)
        result = glGetShaderiv(frag_shader, GL_COMPILE_STATUS)
        if (result != 1): # shader didn't compile
            print("Couldn't compile fragment shader\nShader compilation Log:\n" + str(glGetShaderInfoLog(frag_shader)))

        # Setup your rendering program, attaching the shaders
        self.renderingProgramID = glCreateProgram()
        glAttachShader(self.renderingProgramID, vert_shader)
        glAttachShader(self.renderingProgramID, frag_shader)
        glLinkProgram(self.renderingProgramID)
        #=====================================================================================
        # HANDLES FOR ATTRIBUTES 
        #=====================================================================================
        #----------- POSITION
        self.positionLoc			= glGetAttribLocation(self.renderingProgramID, "a_position") 
        glEnableVertexAttribArray(self.positionLoc)

        #----------- NORMAL
        self.normalLoc			= glGetAttribLocation(self.renderingProgramID, "a_normal") 
        glEnableVertexAttribArray(self.normalLoc)

        #=====================================================================================
        # HANDLES FOR OTHER VARIABLES
        #=====================================================================================
        #----------- MATRICES
        self.modelMatrixLoc			= glGetUniformLocation(self.renderingProgramID, "u_model_matrix")
        self.projectionMatrixLoc	= glGetUniformLocation(self.renderingProgramID, "u_projection_matrix")
        self.viewMatrixLoc			= glGetUniformLocation(self.renderingProgramID, "u_view_matrix")

        # ... and all the other handles you might need

        
    #=====================================================================================
    # USE RENDERING PROGRAM
    #=====================================================================================
    def use(self):
        try:
            glUseProgram(self.renderingProgramID)   
        except OpenGL.error.GLError:
            print(glGetProgramInfoLog(self.renderingProgramID))
            raise

    #=====================================================================================
    # METHODS FOR PASSING VALUES TO THE SHADER THROUGH THE HANDLES
    #=====================================================================================
    #----------- MODEL MATRIX
    def set_model_matrix(self, matrix_array):
        glUniformMatrix4fv(self.modelMatrixLoc, 1, True, matrix_array)

    #----------- VIEW MATRIX
    def set_view_matrix(self, matrix_array):
        glUniformMatrix4fv(self.viewMatrixLoc, 1, True, matrix_array)

    #----------- PROJECTION MATRIX
    def set_projection_matrix(self, matrix_array):
        glUniformMatrix4fv(self.projectionMatrixLoc, 1, True, matrix_array)
    
    #... need a method for all the variables you wish to actually use.
```