Theoretical code, will probably not work directly
```py
class Camera:
    def __init__(self):
        pointeye:Point
        u:Vector
        v:Vector
        n:Vector

    def yaw(angle):
        """Can be built ontop of the roll function

        |cos    0   sin|
        |0      1     0|
        |-sin   0   cos|
        """
        pass

    def pitch(angle):
        """Can be built ontop of the roll function
        
        |1  0       0|
        |0  cos  -sin|
        |0  sin   cos|
        """
        pass

    def roll(angle):
        """
        rotating the camera in place by an angle.
        a = (cos0, sin0)    
        b = (-sin0, cos0)

        |cos  -sin    0|
        |sin  cos     0|
        |0    0       1|
        """
        # x_dimension
        temp_x = u.x
        u.x = cos(angle)*u.x + sin(angle)*v.x
        v.x = cos(angle)*vx - sin(angle) * temp_x

        # y_dimension
        temp_y = u.y
        u.y = cos(angle) *n.y + sin(angle)*v.y
        v.y = cos(angle)* v.y - sin(angle)*temp_y

        # z_dimension
        temp_z = u.z
        u.z = cos(angle) * u.z + sin(angle) * v.z
        v.z = cos(a) * v.z - sin(angle) * temp_z

    def slide(delta_u, delta_v, delta_n):
        """
        delta_n controls how much we're moving forward/backward
        delta_v controls how much we're moving up/down
        delta_u control how much we're moving left/right
        """
        eye.x += delta_n * n.x + delta_v * v.x + delta_u * u.x
        eye.y += delta_n * n.y + delta_v * v.y + delta_u * u.y
        eye.z += delta_n * n.z + delta_v * v.z + delta_u * u.y

    def slide_n(delta_n):
        """Slide in one direction"""
        eye.x += delta_n * n.x
        eye.y += delta_n * n.y
        eye.z += delta_n * n.z


    def build-view-matrix()

```
If the camera is the center of the universe:
* `eye` will be the `(0,0,0)` point of the camera.
* `v`, `u` and `n` are vectors. 
    * `v` is the `y` from the camera's point of view
    * `n` is the `z` from the camera's point of view (pointing away from the back of the camera)
    * `u` is the `x` from the camera's point of view

Then we got different movement actions for a camera in 3D space
* Spin
    > Essentially, if you want to spin around an axis, you exclude that axis from the equation.  (multiplying against the other two vectors)
    * Pitch: like moving your head up and down, around x/u axis
    * Yaw: like moving your head to the sides, around y/v axis
    * Roll: moving around the z/n axis
* Slide: Moving around without  changing the direction or rotation of the camera.

>moving forward while looking at the ground, will we go into the floor? or look around when looking at the floor? will be looking at the ceiling next? or rotate our view while still looking at the ground?
>* Removing an axis would help keep us grounded. In some games we would move slower when looking at the ground, because skipping changing the y axis, then the other axis is alot shorter distance.

Locking the `y` axis on the `eye` point will keep us grounded