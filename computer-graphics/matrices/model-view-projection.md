> [Opengl tutorial: Matrices](http://www.opengl-tutorial.org/beginners-tutorials/tutorial-3-matrices/)

>"Frustum" -> stúfur (þrívíð trapisa)
# Matrices
![Diagram that explains layers of projection](images/MVP.png)

## Model Matrix
Sets the vertices of a model, relative to the object's center.

Using the model matrix when _drawing_ your shapes will give the model's vertices coordinates in the global coordinate system.
>![From model to world space](images/model_to_world.png)
>Going from  __model space__ into the __world space__, translating where the vertices of the model should be in the _world_ (according to the global point of origin).

## View Matrix
>"The engines don't move the ship at all. The ship stays where it is and the engines move the universe around it." - Futurama

So it translates all the coordinates from the __world space__ to be in the __camera space__, because we will see the _world_ from the camera, making the camera the _center of the universe_.

>![Camera space](images/projection-matrix-1.png)
>Red: camera frustum (what it can see), and blue cubes are object models.
>
>This also gives us a good idea of how the camera might __clip__ the scene based on the __near__ and __far__ (things that are too far or too close would not be rendered, as if sliced/clipped by the frustum)

## Projection matrix is the perspective
The __projection matrix__ controls how the camera sees the world, such as perspective and depth.

So multiplying everything according to the _projection_ will warp things according to the camera's perspective. 

So essentially it's how we create the illusion of 3D space in a flat surface, since the screen is actually just a flat surface.


>![Camera space](images/projection-matrix-2.png)
>
>In the image above, the frustum has become a perfect cube (which makes more sense for a flat screen representation of the view). In this particular case the perspective has depth, so the objects closest to the camera are big, while the ones in the back will appear smaller.
>
>![Projected view](images/projected1.png)
>How things look on screen after projection.
