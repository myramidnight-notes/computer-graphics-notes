# Transformations and matrices

## Homogenous Coordinates
When a point and vector are translated to the _homogenous coordinates_ in matrices, then they will have a indicator that tells us if it's a point or vector.

* Points will have a extra number `1`
    ```
    P(Px,Py) =  |Px|
                |Py|
                |1 |
    ```
* Vectors would have a extra number `0`
    ```
    V(Vx,Vy) =  |Vx|
                |Vy|
                |0 |
    ```

same goes for 3D coordinates, we just include the `z`


```
w = point of origin 

|Qx|    |wx + Px * ax   + Py * bx   + Pz * cx   |
|Qy|    |wy + Px * ay   + Py * by   + Pz * cy   |
|Qz| =  |wz + Px * az   + Py * bz   + Pz * cz   |
|1 |    |1  + Px * 0      Py * 0      Pz * 0    |


```