# Computer Graphics
> Keep in mind that anything sent into the OpenGL pipeline, they are not actual shapes that are stored, they are just graphics, static image, they are just directions for rendering, so there might be other program code that keeps track of each object shape, to keep track of it's shape/size/location and such.
>
>[Teacher's __Youtube Channel__](https://www.youtube.com/channel/UCZsPUrqqamZvNk-Ov0YDbJQ) (He teaches in english), it is full of lectures on the topic of computer graphics.

* ### [Videos from lectures and programming sessions](videos.md)

## Index
* ### [Introduction to OpenGL](opengl/README.md)
    > OpenGL makes it easier to get our ideas onto the screen as graphics, both 2D and 3D
    >*  [Basic: Initiate Pygame](opengl/basic_initiate.md)
    >    > Initiate pygame to open a window and draw a shape with opengl
    >*  [Drawing primitives](drawing_shapes.md) 
* ### [Window-2-Viewport and Vectors](vectors/README.md)
    > Mapping the world to the screen
    >* [Vector math for graphics (pdf slides)](pdf/TGRA_Slides_Vectors.pdf)
    >* [Viewport Mapping (pdf)](pdf/TGRA_WindowViewportMapping.pdf)
    >* Formula sheet: [__front__](pdf\TGRA_FormulaSheet_Front.pdf) and [__back__](pdf\TGRA_FormulaSheet_Back.pdf) (pdf)
* ### Intersection and reflection
    > How things hit and bounce on surfaces
    >* [video lecture](https://www.youtube.com/watch?v=CAa176ClmhY)
    >* [Intersection/Reflection formulas (pdf)](pdf/TGRA_Intersection_Reflection_Formulas.pdf) 
* ### Transformations
    > Moving and rotating models in the world
    >* [Transformations (pdf slides)](pdf/TGRA_Slides_Transformations.pdf)
* ### [Matrices](matrices/README.md)
    >The camera and perspective. Model matrix, View matrix, Projection matrix. 
    >* [Camera Projection (pdf slides)](pdf/TGRA_Slides_CameraProjection.pdf)
* ### [Shading and Colors](shading/README.md)
    > How shading manages to make things look 3D (light effects intencity of colors on surfaces, creating shading)

* ### [Clipping](clipping/README.md)
    >How to just process and render what's within view of the camera?
* ### 3D meshes
* ### Rasterization, depth test
    >* [Rasterization (pdf slides)](pdf/TGRA_Rasterization.pdf)
* ### Smooth curves & motion
* ### Blending & Fragment operations
* ### Pipeline overview

## Other links
* [docs.GL](https://docs.gl/)
    >Good OpenGL documentation site, list of GL functions, and we can select what version of OpenGL we're using (we can see in what versions have implemented that function)
* [3rd person camera (video)](https://www.youtube.com/watch?v=PoxDDZmctnU)