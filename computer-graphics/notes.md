
* glTranslate2f(x,y)   
    * glTranslate3f(x,y,z)
    >* item is centered at this point
    >* Changing the center of the 'universe' for our frame
* glScale(x,y,z)
    >* Don't scale first, it makes things messed up (else we would be translating within the scaled coordinate system)
    >* We're changing the model matrix
* drawShape()
    >We draw things after we're done translating and scaling.
* The vertices for the shapes should not reflect the size and position they will take on the screen, we should let the program handling changing the scale and position.

1. So first we translate where our center point is
1. Then we decide our rotation if needed
1. The scale of our grid
1. Finally we draw the shape, which will be using the translated and scaled grid.


# Matrix
* `glPushMatrix()`
    > To wrap any transformations/scale we want to do to draw new objects.
* `glPopMatrix()`



## Notes:
* [3D isometric sprites](https://www.youtube.com/watch?v=WKohoRRwgO4)
    >Just some musings about making a isometric game/grid that is actually 3D

* [Planet earth textures for modeling](https://www.shadedrelief.com/natural3/pages/textures.html)