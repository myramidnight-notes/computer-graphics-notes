# Collission response - reflection, intersection

### Videos
* [Video lecture on the topic](https://www.youtube.com/watch?v=CAa176ClmhY)
* [Collissions - detection and response, intersection and reflection](https://www.youtube.com/watch?v=KVpnwP9bUP0)
* [Solutions to lab problems: vectors, intersections and reflections](https://www.youtube.com/watch?v=JHy78pHiv2o)

## Formulas
