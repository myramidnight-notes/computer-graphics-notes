* [Slides about vectors.pdf](../pdf/TGRA_Slides_Vectors.pdf)
# Vectors
>* Adding, Subtracting and Multiplying vectors
>* Negative vectors
>* Dot and Cross products
>* Angle between vectors

## Coordinate systems
There are a few ways the coordinates are organised, based on dimensions in graphics
>![Cordinate systems](images/vector-coordinates.png)
>Seems that __left-hand__ system would increase `z` as it gets closer to us, while __right-hand__ system increases the `z` as it gets further away.
* Point of origin: `(0,0,0)`
* Axis represented as vectors:
    * _i_ = `(1,0,0)`
    * _j_= `(0,1,0)`
    * _k_=`(0,0,1)`

## Points and Vectors
>![](images/vector-points.png)

## Adding vectors
>![](images/vector-adding.png)

## Subtracting vectors
>![](images/vector-subtract.png)

## Multiplying vectors
>Making the vector longer
>![](images/vector-multiply.png)

## Negative vector (reversing direction)
>Reversing the direction of the vector
>![](images/vector-negative.png)


## Dot product of vectors
>A vector has __magnitude__ (length) and __direction__. 
>* They can be __multiplied__ using the __dot product__ (also known as __Cross-product__)
>* The dot product of two vectors is a number, not another vector.
>
>![](images/dot-product-idea.png)
>* Works the same with any number of dimensions
>   * _a&compfn;b = a<sub>x</sub>b<sub>x</sub> + a<sub>y</sub>b<sub>y</sub>_
>   * So to extend that to 3D vectors: _a&compfn;b = a<sub>x</sub>b<sub>x</sub> + a<sub>y</sub>b<sub>y</sub> + a<sub>z</sub>b<sub>z</sub>_
>* [MathIsFun: Dot product](https://www.mathsisfun.com/algebra/vectors-dot-product.html)
>### Calculating dot-product with angle
>![](images/dot-product-angle.png)
>* __a&compfn;b__ = |a| * |b| * cos(&theta;)
>   >__|a|__ and __|b|__ is the length of the vectors, and &theta; is angle between the vectors.
>### Properties of the dot product
>![](images/vector-dot-product-properties.png)
>* Doesn't matter which vector comes first. 

## Angle between vectors
>Any 2D vector can be represented as:
>* v = |v| (cos&theta;, sin&theta;) = (|v| cos&theta;, |v| sin&theta;)
>   >Using the angle between the vectors and the x-axis
>
>To find a correlation between the _dot product_ and the _angle_ between vectors, lets look at two vectors, _b_ and _c_.
>* b = (|b| cos&theta;<sub>b</sub>, |b| sin&theta;<sub>b</sub>)
>* c = (|c| cos&theta;<sub>c</sub>, |c| sin&theta;<sub>c</sub>)
>
>Their dot product:
>![](images/vector-angle.png)
>### Interesting properties (dot-product and angles)
>Connection between dot-products and the angles. &theta; is the angle between the vectors. 
>* if ___a&compfn;b_ &gt; 0__ then __&theta;&lt;90&deg;__
>* if ___a&compfn;b_ &lt; 0__ then __&theta;&gt;90&deg;__
>* if ___a&compfn;b_ = 0__ then __&theta; = 90&deg;__

## Cross Product (multiplying with dot-product)
>The cross product __a &times; b__ of two vectors is __another vector__ that is at 90&deg; angle to both bectors _a_ and _b_. 
>
>A Perpendicular vector!
>
>![](images/cross-product.gif)
>
>* a&times;b = (a<sub>y</sub>b<sub>z</sub> - a<sub>z</sub>b<sub>y</sub>)i + (a<sub>z</sub>b<sub>x</sub> - a<sub>x</sub>b<sub>z</sub>)j + (a<sub>x</sub>b<sub>y</sub> - a<sub>y</sub>b<sub>x</sub>)k
>
>or simplified: __let's find _(c<sub>x</sub> , c<sub>y</sub> , c<sub>z</sub>)___
>* _c<sub>x</sub>_ = a<sub>y</sub>b<sub>z</sub> - a<sub>z</sub>b<sub>y</sub>
>* _c<sub>y</sub>_ = a<sub>z</sub>b<sub>x</sub> - a<sub>x</sub>b<sub>z</sub>
>* _c<sub>z</sub>_ = a<sub>x</sub>b<sub>y</sub> - a<sub>y</sub>b<sub>x</sub>
>
>![](images/cross-product-calculate.gif)
>### Properties of cross product
>* a&compfn; (a&times;b) = b &compfn; (a&times;b) = 0 
>* i &times; j = k
>* j &times; k = i
>* k &times; i = j
>    * `i` being the global `x` vector
>    * `j` being the global `y` vector
>    * `k` being the global `z` vector
>* a &times; b = -b &times; a
>* a &times; (b+c) = a &times; b + a &times; c
>* (s * a)&times; b = s * (a &times; b)