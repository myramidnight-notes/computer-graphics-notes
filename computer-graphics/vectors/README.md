> Check the `pdf` directory for useful notes as well.

* ### [Vectors](vectors.md)
    > Formulas and calculations
* ### Window-2-Viewport
    * [lecture: W2V mapping and vector operations](https://www.youtube.com/watch?v=U9cY0YxpAVs)
    * [Vectors](https://www.youtube.com/watch?v=PF0yb33OqNc)
    * [same lecture, windows, viewports and vectors](https://youtu.be/gW_U9WjYciY)
    * [solutions to w2v problems](https://www.youtube.com/watch?v=EQ0wXFuwYCE)

