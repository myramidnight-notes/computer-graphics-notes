
from OpenGL.GL import *
from OpenGL.GLU import *
from math import *
from OpenGL.constant import FloatConstant

import pygame
from pygame.locals import *

import sys
import time

from shaders import Light, Material, Color, Shader3D
from assets import TextureHandler,KeyInputHandler, MaterialHandler,ObjectHandler
from shapes import *
from matrices import *


#========================================================================
# The Light Program
#========================================================================
class LightsStore():
    def __init__(self):
        self.spin = Light(Point(0,0,0),Color(1,1,1), Color(0.3,0.3,0.3))        
        self.player = Light(Point(0,0,0),Color(0.4, 0.8, 0), Color(0.3,0.3,0.3))   
    def get_lights(self):
        return [self.spin, self.player]

#========================================================================
# The Shapes Store
#========================================================================
class ShapesStore(ObjectHandler):
    def __init__(self):
        super().__init__("assets/models/example/")
        self.cube = Cube()
        self.sphere = Sphere()
        #self.sprite = Sprite()
        self.sphere_metal = self.load_mesh('metallic_sphere.obj')
        self.combined_model = self.load_mesh('combined_model.obj')
        self.box_metal = self.load_mesh('metallic_box.obj')
        self.blob = self.load_mesh('combined_model.obj')

#========================================================================
# The Texture store
#========================================================================
class TextureStore(TextureHandler):
    """Stores textures, the parent TextureHandler provides the load textures method"""
    def __init__(self,shader:Shader3D):
        """Takes in the shader so we don't have to pass it through when using textures"""
        super().__init__("assets/textures/example/")
        self.shader = shader
        self.wooden_dice = self.load_texture("wooden_dice.jpg")
        self.wooden_crate = self.load_texture("wooden_crate.jpg")
        self.planet_day = self.load_texture("planet/planet_no_clouds.jpg")
        self.planet_water_mask = self.load_texture("planet/water_mask.png")
        #self.starry_sky = self.load_texture("planet/starry_sky.jpg")
        self.cubemap = self.load_cubemap("wooden_dice.jpg","wooden_dice.jpg","wooden_dice.jpg","wooden_dice.jpg","wooden_dice.jpg","wooden_dice.jpg")

    # def diffuse(self, texture:int):
    #     """Sets the ActiveTexture and BindTexture for rendering
    #     \nFor the basic texture to apply to object"""        
    #     glActiveTexture(GL_TEXTURE0)
    #     glBindTexture(GL_TEXTURE_2D, texture) 
    #     self.shader.set_diffuse_texture()

    # def specular(self, texture:int):
    #     """Sets the ActiveTexture and BindTexture for rendering
    #     \nJust this texture would be used for specular mapping"""        
    #     glActiveTexture(GL_TEXTURE1)
    #     glBindTexture(GL_TEXTURE_2D, texture) 
    #     self.shader.set_texture_specular()


class CameraStore:
    def __init__(self):
        self.player = ViewMatrix()
        self.player.look(Point(3,0,3), Point(0,0,0), Vector(0,1,0))
        self.tail = ViewMatrix()
        self.tail.look(Point(3,0,1), Point(-3,0,0), Vector(0,1,0))

        self.active_camera = self.player

    def get_active_camera(self):
        return self.active_camera

    def toggle_camera(self):
        """Will roll through the available cameras"""
        if self.active_camera == self.player:
            self.active_camera = self.tail
        else:
            self.active_camera = self.player

class MaterialStore(MaterialHandler):
    def __init__(self,shader,textures:TextureStore):
        super().__init__(shader)  
        self.__textures = textures    
        # Planet earth
        self.planet_day = Material(self.colors['white'], Color(.1,.1,.1), 10)
        self.planet_day.diffuse.a = 0.9
        self.planet_day.set_textures(
            diffuse = self.__textures.planet_day,
            specular = self.__textures.planet_water_mask
        )


    def use(self, material:Material):
        """Set the material color, specularity and shiny, 
        \nthe material store has some preset color values you can use
        \n[materials.use(materials.orange)] if the store is called materials for example"""
        self._shader.set_material(material)
        if material.texture_diffuse is not None: 
            glActiveTexture(GL_TEXTURE0)
            glBindTexture(GL_TEXTURE_2D, material.texture_diffuse) 
            self._shader.set_diffuse_texture()
        if material.texture_specular is not None:     
            glActiveTexture(GL_TEXTURE1)
            glBindTexture(GL_TEXTURE_2D, material.texture_specular) 
            self._shader.set_texture_specular()
        if material.texture_alpha is not None:
            pass

    def reset(self):
        self._reset_texture_layer(0)
        self._reset_texture_layer(1)

        self._shader.reset_diffuse_texture()
        self._shader.reset_specular_texture()
        return super().reset()  

#========================================================================
# The Graphics Program
#========================================================================
class GraphicsProgram3D:
    def __init__(self):
        pygame.init() 
        pygame.display.set_mode((800,600), pygame.OPENGL|pygame.DOUBLEBUF)

        # Game clock
        self.clock = pygame.time.Clock()
        self.clock.tick()

        # General settings
        self.fov = pi/2
        self.angle = 0
        self.grounded = True
        
        # ========== Model Matrix    ==============================
        self.model_matrix = ModelMatrix()

        # ========== Cameras    ==============================
        self.cameras = CameraStore() 

        # ======= Projection Matrix ==============================
        self.projection_matrix = ProjectionMatrix()
        #self.projection_matrix.set_orthographic(-3,3,-3,3, 0.5,10)
        self.projection_matrix.set_perspective(self.fov,800/600, 0.5, 20)

        # ========== Shader          ==============================
        self.shader = Shader3D()
        self.shader.use()
        
        self.shader.set_projection_matrix(self.projection_matrix.get_matrix())
        self.shader.set_fog(0.5,8,0,0,0)

        # ========== Stores (to keep things) ==============================
        self.active_keys = KeyInputHandler()
        self.lights = LightsStore()    
        self.textures = TextureStore(self.shader)
        self.materials = MaterialStore(self.shader, self.textures)
        self.shapes = ShapesStore()
        
        #  ========== Lighting   ==============================
        #self.shader.set_light_position(Point(0,10,0)) # I am the light!
        # ambient light
        self.shader.set_ambient(1,1,1, 0.5)

        self.frame_sum = 0;
        self.frame_ticker = 0;
    

    #=====================================================================================
    # CAMERA AND MOVEMENT
    #=====================================================================================
    def player_moving(self, delta_time):
        #Moving
        currMovement_n = 0;
        currMovement_u = 0;
        if self.active_keys.K_w:
            currMovement_n += -1 
        if self.active_keys.K_s:
            currMovement_n += 1 
        if self.active_keys.K_a:
            currMovement_u += -1 
        if self.active_keys.K_d:
            currMovement_u += 1 

        self.cameras.player.slide(del_u = (currMovement_u ) * delta_time)
        self.cameras.player.slide(del_n = (currMovement_n ) * delta_time)

    def camera_moving(self, delta_time):
        #Rotations
        if not self.grounded:
            if self.active_keys.K_q:
                self.cameras.player.roll(pi * (delta_time/2))
            if self.active_keys.K_e:
                self.cameras.player.roll(-pi * (delta_time/2))

        if self.active_keys.K_UP:
            self.cameras.player.pitch(pi * (delta_time/2))
        if self.active_keys.K_DOWN:
            self.cameras.player.pitch(-pi * (delta_time/2))
        if self.active_keys.K_LEFT:
            self.cameras.player.yaw(pi * (delta_time/2))
        if self.active_keys.K_RIGHT:
            self.cameras.player.yaw(-pi * (delta_time/2))

    def camera_zoom(self, delta_time):
        #Changing FOV (zooming)
        if self.active_keys.K_t:
            self.fov -= 0.25 * delta_time
        if self.active_keys.K_g:
            self.fov += 0.25 * delta_time

    def framerate_ticker(self, delta_time):
        """Prints the framerate every second"""
        self.frame_sum += delta_time
        self.frame_ticker += 1
        if self.frame_sum > 1:
            print(self.frame_ticker / self.frame_sum)
            self.frame_sum = 0
            self.frame_ticker = 0

    #=====================================================================================
    # UPDATE
    #=====================================================================================
    def update(self):
        delta_time = self.clock.tick() / 1000.0
        self.shader.set_eye_position(self.cameras.get_active_camera().eye)
        self.lights.spin.position =Point( cos(self.angle) * 10 , 5,  sin(self.angle)*10)
        self.lights.player.position = self.cameras.player.eye

        self.framerate_ticker(delta_time)

        self.angle += pi * delta_time
        # if angle > 2 * pi:
        #     angle -= (2 * pi)

        #Update the tail camera to follow 

        self.player_moving(delta_time)
        self.camera_moving(delta_time)
        self.cameras.tail.look(self.cameras.tail.eye, self.cameras.player.eye, Vector(0,1,0))

        self.camera_zoom(delta_time)

    #=====================================================================================
    # DRAW FUNCTIONS
    #=====================================================================================
    def draw_rotating_cube(self, x=0, y=0, z=0):
        self.model_matrix.push_matrix()
        self.model_matrix.add_translation(x,y,z) 
        self.model_matrix.add_rotation_x(self.angle)
        self.shader.set_model_matrix(self.model_matrix.matrix)
        self.shapes.cube.draw(self.shader)
        self.model_matrix.pop_matrix()

    def draw_lots_of_spheres(self):
        """8 spheres rotating around a center"""
        for i in range(8):
            self.model_matrix.push_matrix()
            self.model_matrix.add_rotation_x(self.angle * 0.74324 + i * pi / 4)
            self.model_matrix.add_translation(0,5,0)
            self.model_matrix.add_rotation_x(-(self.angle * 0.74324 + i * pi / 4))
            self.model_matrix.add_scale(3,3,3)
            self.shader.set_model_matrix(self.model_matrix.matrix)
            self.shapes.sphere.draw(self.shader)
            self.model_matrix.pop_matrix()

    def draw_scaled_cube(self, x=0, y=0, z=0):
        self.model_matrix.add_translation(x,y, z) 
        self.model_matrix.push_matrix()
        curr_size = (x*0.8)+1
        self.model_matrix.add_scale(x=curr_size,z=curr_size,y=curr_size)
        self.shader.set_model_matrix(self.model_matrix.matrix)
        self.shapes.cube.draw(self.shader)
        self.model_matrix.pop_matrix()

    def draw_simple_cube(self, x=0, y=0, z=0):
        self.model_matrix.push_matrix()
        self.model_matrix.add_translation(x,y, z) 
        self.shader.set_model_matrix(self.model_matrix.matrix)
        self.shapes.cube.draw(self.shader)
        self.model_matrix.pop_matrix()

    def draw_earth(self, x=0, y=0, z=0):
        self.materials.use(self.materials.planet_day)
        
        #self.textures.diffuse(self.materials.planet_day.texture_diffuse)
        #self.textures.specular(self.materials.planet_day.texture_specular)

        self.model_matrix.push_matrix()
        self.model_matrix.add_translation(x,y,z) 
        self.model_matrix.add_scale(2,2,2) 
        self.model_matrix.add_rotation_y(self.angle * 0.2) #spin
        self.model_matrix.add_rotation_z(pi) #so it's not upside down
        self.shader.set_model_matrix(self.model_matrix.matrix)
        self.shapes.sphere.draw(self.shader)

        self.model_matrix.pop_matrix()

        self.materials.reset()


    def draw_player(self, x=0, y=0, z=0):
        self.model_matrix.push_matrix()
        self.model_matrix.add_translation(x,y,z) 
        self.model_matrix.add_scale(.5,.5,.5)
        self.shader.set_model_matrix(self.model_matrix.matrix)
        self.shapes.combined_model.draw(self.shader)
        self.model_matrix.pop_matrix()
    #=====================================================================================
    # DISPLAY
    #=====================================================================================
    def display(self):
        ### GL_DEPTH_TEST: So that 3D renders normally, 
        # interesting effect if disabled 
        glEnable(GL_DEPTH_TEST)  
        glClearColor(0.0, 0.0, 0.0, 1.0)

        ### --- YOU CAN ALSO CLEAR ONLY THE COLOR OR ONLY THE DEPTH --- ###
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT)  

        glViewport(0, 0, 800, 600)

        #sets the shader at each frame
        #Update the perspective when needed
    #if self.active_keys.K_t or self.active_keys.K_g:
        self.projection_matrix.set_perspective(self.fov,800/600, 0.5, 20)        
        self.shader.set_projection_matrix(self.projection_matrix.get_matrix())

        # Set the camera that should display on the screen
        self.shader.set_view_matrix(self.cameras.get_active_camera().get_matrix())

        #self.shader.set_diffuse_texture(self.textures['wooden_dice'])

        self.shader.set_lights([self.lights.spin, self.lights.player])
        self.model_matrix.load_identity()

        #============================= BEGIN DRAWING =======================================

        self.materials.use(self.materials.green) #colors all shapes from this point
        self.draw_rotating_cube(2)
        self.materials.reset()

        self.materials.use(self.materials.orange)
        self.draw_rotating_cube(-2)        
        self.materials.reset()
        
        if self.cameras.get_active_camera() == self.cameras.tail:
            player_pos = self.cameras.player.eye
            self.materials.use(self.materials.purple)
            self.draw_player(player_pos.x, player_pos.y, player_pos.z)
                    
            self.materials.reset()

        glEnable(GL_BLEND)
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
        self.draw_earth()
        glDisable(GL_BLEND)        

        #============================= END DRAWING =======================================
        pygame.display.flip()

    #=====================================================================================
    # PROGRAM LOOP
    #=====================================================================================
    def program_loop(self):
        exiting = False
        while not exiting:

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    print("Quitting!")
                    exiting = True
                else:                    
                    camera_toggle = self.active_keys.K_c

                    self.active_keys.key_input(event)

                    #Did we press the ESC key? Quit the game
                    if self.active_keys.K_ESCAPE == True:
                        print("Exiting!")
                        exiting = True

                    # check if the camera toggle key was pressed
                    if camera_toggle == True and self.active_keys.K_c == False:
                        self.cameras.toggle_camera()
                
            self.update()
            self.display()

        #OUT OF GAME LOOP
        pygame.quit()

    def start(self):
        self.program_loop()

if __name__ == "__main__":
    GraphicsProgram3D().start()