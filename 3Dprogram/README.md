# My base 3D program
This program has been built on code lab sessions (videos) slowly improving the program and how to do things. My program looks totally different from the examples because I quickly separted classes to their own files and directories, otherwise the code itself is built on the same things as the videos explained.

## What has been implemented?
* Binding vertex/normal/uv arrays data to buffers, to be referenced by ID
    >It improved framerate, not having to process the arrays every frame,
    >but instead be able to load it as static data that the program had stored
* Textures!
    >The shader can be told what texture layer to process in what ways, to allow
    >us to have diffuse texture, specular texture, alpha texture...
    >
    >My simple3D shader takes into account if the material alpha is less than 1,
    >then it will render alpha based on texture (using it as a map)
* Various structures
    >Since the shaders are based on the C language (it is just OpenGL language though, not all C or C++ functionality is available, as far as I know), it is nice to know that you can use the `struct` to group values together like an object, such as material structure, light structure... 
* Mutiple lights
    >It can now process multiple light sources, it shader takes a list of Light objects and will only process up to the max number of lights (ignoring any that go past that)
* Fog!
    >It has fog