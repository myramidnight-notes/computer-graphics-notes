/*
This is the first shader, so it does not work passing varying variables from fragment shader to the vertex shader, that's like throwing information up stream.
*/

#define MAX_NUMBER_OF_LIGHTS 2
//====================== ATTRIBUTE VARIABLES ======================
attribute vec3 a_position;
attribute vec3 a_normal; 
attribute vec2 a_uv;

//====================== UNIFORM VARIABLES ======================
//Matrices
uniform mat4 u_model_matrix;
uniform mat4 u_view_matrix;
uniform mat4 u_projection_matrix;

//Lighting
uniform vec4 u_eye_position;
uniform vec4 u_lights_position[MAX_NUMBER_OF_LIGHTS];

//====================== VARYING VARIABLES ======================
varying vec4 v_normal;
varying vec4 v_s[MAX_NUMBER_OF_LIGHTS];
varying vec4 v_h[MAX_NUMBER_OF_LIGHTS];

//Textures / UV
varying vec2 v_uv;
varying float v_distance;
varying vec4 v_v;

// VERTEX SHADER
void main(void)
{
	/*=====================================================================================
	** ATTRIBUTES
	**=====================================================================================*/
	vec4 position = vec4(a_position.x, a_position.y, a_position.z, 1.0); //1 means it's a point
	vec4 normal = vec4(a_normal.x, a_normal.y, a_normal.z, 0.0); // 0 means it's a vertex
	v_uv = a_uv;


	/*---- LOCAL COORDINATES -------------------------------------------------------------*/
	position = u_model_matrix * position;
	v_normal = normalize(u_model_matrix * normal); //Normalizing is dividing by their length
	v_v = normalize(u_eye_position - position);
	
	/*---- TEXTURE -----------------------------------------------------------------------*/
	
	/*---- GLOBAL COORDINATES ------------------------------------------------------------*/
	
	for (int i = 0; i < MAX_NUMBER_OF_LIGHTS; i++){
		v_s[i] = normalize(u_lights_position[i] - position); //vector from point to the light
		v_v = normalize(u_eye_position - position);
		//v_h[i] = normalize(v_s[i] + v);
	}

	/*---- EYE COORDINATES ---------------------------------------------------------------*/
	position = u_view_matrix * position;
	v_distance = -position.z;

	/*---- CLIP COORDINATES --------------------------------------------------------------*/
	position = u_projection_matrix * position;

	/*=====================================================================================
	** FEED POSITION TO OPENGL
	**=====================================================================================*/
	gl_Position = position;
}