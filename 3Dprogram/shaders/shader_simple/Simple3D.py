import sys

from OpenGL.GL import *
import OpenGL.GLU
from matrices import *
from shaders import Light, Material
from typing import List
from shaders.ShaderBase import ShaderBase


class Shader3D(ShaderBase):
    def __init__(self):
        vert_shader = sys.path[0] + "/shaders/shader_simple/simple3D.vert"
        frag_shader = sys.path[0] + "/shaders/shader_simple/simple3D.frag"
        super().__init__(vert_shader, frag_shader)
        #=====================================================================================
        # HANDLES FOR ATTRIBUTES 
        #=====================================================================================
        #----------- POSITION
        self.positionLoc			= glGetAttribLocation(self.renderingProgramID, "a_position") 
        glEnableVertexAttribArray(self.positionLoc)

        #----------- NORMAL
        self.normalLoc			= glGetAttribLocation(self.renderingProgramID, "a_normal") 
        glEnableVertexAttribArray(self.normalLoc)

        #----------- TEXTURES / UV
        self.uvLoc			= glGetAttribLocation(self.renderingProgramID, "a_uv") 
        glEnableVertexAttribArray(self.uvLoc)

        #=====================================================================================
        # HANDLES FOR OTHER VARIABLES
        #=====================================================================================
        #----------- MATRICES
        self.modelMatrixLoc			= glGetUniformLocation(self.renderingProgramID, "u_model_matrix")
        self.projectionMatrixLoc	= glGetUniformLocation(self.renderingProgramID, "u_projection_matrix")
        self.viewMatrixLoc			= glGetUniformLocation(self.renderingProgramID, "u_view_matrix")

        #----------- LIGHTING / SHADING / COLORS
        self.eyePosLoc              = glGetUniformLocation(self.renderingProgramID, "u_eye_position")

        self.ambientDiffuseLoc  	= glGetUniformLocation(self.renderingProgramID, "u_ambient_diffuse")
        self.ambientStrength  	    = glGetUniformLocation(self.renderingProgramID, "u_ambient_strength")

        self.usingFogLoc              = glGetUniformLocation(self.renderingProgramID, "u_fog")
        self.fogStartLoc  	        = glGetUniformLocation(self.renderingProgramID, "u_fog_start")
        self.fogEndLoc  	        = glGetUniformLocation(self.renderingProgramID, "u_fog_end")
        self.fogColorLoc            = glGetUniformLocation(self.renderingProgramID, "u_fog_color")

    
        # Just make sure that this number is the same as the max_lights in shaders are
        self.max_lights:int         = 2       

        #self.lights                 = glGetUniformLocation(self.renderingProgramID, "u_lights.")
        self.lightPosLoc			=[]
        self.lightDiffuseLoc	    =[]
        self.lightSpecularLoc	    =[]
        for i in range(self.max_lights):
            self.lightPosLoc.append(glGetUniformLocation(self.renderingProgramID, "u_lights_position[{:d}]".format(i)))
            self.lightDiffuseLoc.append(glGetUniformLocation(self.renderingProgramID, "u_lights[{:d}].diffuse".format(i)))
            self.lightSpecularLoc.append(glGetUniformLocation(self.renderingProgramID, "u_lights[{:d}].specular".format(i)))

        self.materialDiffuseLoc		= glGetUniformLocation(self.renderingProgramID, "u_mat.diffuse")
        self.materialSpecularLoc	= glGetUniformLocation(self.renderingProgramID, "u_mat.specular")
        self.materialShinyness	    = glGetUniformLocation(self.renderingProgramID, "u_mat.shinyness")

        self.usingTextureLoc		= glGetUniformLocation(self.renderingProgramID, "u_using_texture")
        self.usingSpecularLoc		= glGetUniformLocation(self.renderingProgramID, "u_using_specular")
        
        self.diffuseTextureLoc		= glGetUniformLocation(self.renderingProgramID, "u_texture")
        self.specularTextureLoc		= glGetUniformLocation(self.renderingProgramID, "u_texture_specular")


    #=====================================================================================
    # USE RENDERING PROGRAM
    #=====================================================================================
    def use(self):
        """Can't use the shader without using it"""
        try:
            glUseProgram(self.renderingProgramID)   
        except OpenGL.error.GLError:
            print(glGetProgramInfoLog(self.renderingProgramID))
            raise

    #=====================================================================================
    # MATRICES
    #=====================================================================================
    #----------- MODEL MATRIX
    def set_model_matrix(self, matrix_array):
        glUniformMatrix4fv(self.modelMatrixLoc, 1, True, matrix_array)

    #----------- VIEW MATRIX
    def set_view_matrix(self, matrix_array):
        glUniformMatrix4fv(self.viewMatrixLoc, 1, True, matrix_array)

    #----------- PROJECTION MATRIX
    def set_projection_matrix(self, matrix_array):
        glUniformMatrix4fv(self.projectionMatrixLoc, 1, True, matrix_array)


    #=====================================================================================
    # ATTRIBUTES 
    #=====================================================================================
    #-------- POSITION 
    def set_position_attribute(self, vertex_array):
        """Tells the shader where all the vertices are to draw the polygons"""
        glVertexAttribPointer(self.positionLoc, 3, GL_FLOAT, False, 0, vertex_array)

    #-------- NORMAL 
    def set_normal_attribute(self, vertex_array):
        """Tells the shader the normal of each positional vertex"""
        glVertexAttribPointer(self.normalLoc, 3, GL_FLOAT, False, 0, vertex_array)

    #-------- TEXTURE / UV 
    def set_uv_attribute(self, vertex_array):
        """Tells the shader how to map the texture onto each vertex"""
        glVertexAttribPointer(self.uvLoc, 2, GL_FLOAT, False, 0, vertex_array)

    #----------- BUFFERS
    def set_attribute_buffers_with_normals(self, vertex_buffer_id:int):
        """Yet another version to input the vertices, this time we include the normals in the same array
        \ngroups of 6 numbers (3 being vertex, 3 being normal)"""
        glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_id)
        # 6* sizeof(GLfloat) is the stride, so it will take steps of 6 numbers, then read 3, then some GLU ctypes void pointers
        glVertexAttribPointer(self.positionLoc, 3, GL_FLOAT, False, 6* sizeof(GLfloat), OpenGL.GLU.ctypes.c_void_p(0))
        glVertexAttribPointer(self.normalLoc, 3, GL_FLOAT, False, 6* sizeof(GLfloat), OpenGL.GLU.ctypes.c_void_p(3 * sizeof(GLfloat)))
  
    def set_attribute_buffers_with_uv(self, vertex_buffer_id:int):
        """Yet another version to input the vertices, this time we include the normals in the same array
        \nadds uv coordinates ontop of the set_attribute_buffers_with_normals"""
        glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_id)
        # 6* sizeof(GLfloat) is the stride, so it will take steps of 6 numbers, then read 3, then some GLU ctypes void pointers
        glVertexAttribPointer(self.positionLoc, 3, GL_FLOAT, False, 8* sizeof(GLfloat), OpenGL.GLU.ctypes.c_void_p(0))
        glVertexAttribPointer(self.normalLoc, 3, GL_FLOAT, False, 8* sizeof(GLfloat), OpenGL.GLU.ctypes.c_void_p(3 * sizeof(GLfloat)))
        glVertexAttribPointer(self.uvLoc, 2, GL_FLOAT, False, 8* sizeof(GLfloat), OpenGL.GLU.ctypes.c_void_p(6 * sizeof(GLfloat)))
    #=====================================================================================
    # LIGHTING / SHADING / COLOR
    #=====================================================================================

    #----------- AMBIENT
    def set_ambient(self, red:float, green:float, blue:float, strength:float):
        """Sets the color of the ambient light: red, green, blue"""
        glUniform4f(self.ambientDiffuseLoc,red,green,blue,1.0)
        glUniform1f(self.ambientStrength,strength)

    def set_fog(self, start:float, end:float, red:float, green:float, blue:float):
        """Enable and configure the fog"""
        glUniform1i(self.usingFogLoc, 1)
        glUniform4f(self.fogColorLoc,red,green,blue,1.0)
        glUniform1f(self.fogStartLoc,start)
        glUniform1f(self.fogEndLoc,end)

    def reset_fog(self):
        """To be able to disable the fog being rendered"""
        glUniform1i(self.usingFogLoc, 0)


    #----------- LIGHT
    def set_lights(self,lights: List[Light]):
        """Goes through a list of lights and feeds them to the shader, 
        \nit will process up to the set max lights"""
        for i in range(self.max_lights):
            if (i < len(lights)):
                #while there are lights in the list
                glUniform4f(self.lightPosLoc[i],lights[i].position.x,lights[i].position.y,lights[i].position.z,1.0)
                glUniform4f(self.lightDiffuseLoc[i],lights[i].diffuse.r,lights[i].diffuse.g,lights[i].diffuse.b,1.0)
                glUniform4f(self.lightSpecularLoc[i],lights[i].specular.r,lights[i].specular.g,lights[i].specular.b,1.0)
            else:
                #use default no light
                glUniform4f(self.lightPosLoc[i],0,0,0,0)
                glUniform4f(self.lightDiffuseLoc[i],0,0,0,0)
                glUniform4f(self.lightSpecularLoc[i],0,0,0,0)

    #----------- MATERIAL
    def set_material(self,material:Material):
        """Sets the material to be rendered"""
        glUniform4f(self.materialDiffuseLoc,material.diffuse.r,material.diffuse.g,material.diffuse.b,material.diffuse.a)
        glUniform4f(self.materialSpecularLoc,material.specular.r,material.specular.g,material.specular.b,material.specular.a)
        glUniform1f(self.materialShinyness,material.shininess)
        if material.has_texture:
            glUniform1i(self.usingTextureLoc, 1)
            glUniform1i(self.diffuseTextureLoc,0)
            if material.texture_specular is not None:
                glUniform1i(self.usingSpecularLoc, 1)
                glUniform1i(self.specularTextureLoc,1)


    #----------- EYE POSITION (FOR SPECULARITY)
    def set_eye_position(self, pos):
        """Sets the eye position to calculate reflectiveness of materials"""
        glUniform4f(self.eyePosLoc,pos.x,pos.y,pos.z,1.0)

    #=====================================================================================
    # TEXTURES 
    #=====================================================================================

    def set_diffuse_texture(self):
        """Tell the shader to use GL_TEXTURE1 for diffuse"""
        glUniform1i(self.usingTextureLoc, 1)
        glUniform1i(self.diffuseTextureLoc,0)

    def reset_diffuse_texture(self):
        glUniform1i(self.usingTextureLoc, 0)


    def set_texture_specular(self):
        """Tell the shader to use GL_TEXTURE2 for specularity"""
        glUniform1i(self.usingSpecularLoc, 1)
        glUniform1i(self.specularTextureLoc,1)

    def reset_specular_texture(self):
        glUniform1i(self.usingSpecularLoc, 0)