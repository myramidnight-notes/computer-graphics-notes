
#define MAX_NUMBER_OF_LIGHTS 2

uniform vec4 u_mat_diffuse; 	//material diffuse color
uniform vec4 u_light_diffuse;	// light diffuse color
uniform int u_fog;		//bool for fog

struct Light{
	vec4 position;  
	vec4 diffuse;
	vec4 specular;

	float strength;

	//Directional light
	bool dir;
	vec4 direction;
};

struct Material{
	vec4 diffuse;
	vec4 specular;
	float shinyness;
};

uniform Light u_lights[MAX_NUMBER_OF_LIGHTS];
uniform Material u_material;

//For ambient
uniform vec4 u_ambient_diffuse;
uniform float u_ambient_strenght;

//Light specularity
uniform Material u_mat;


//For the textures
uniform int u_using_texture; 	//bool for textures
uniform int u_using_specular; //for specular texture
uniform sampler2D u_texture;
uniform sampler2D u_texture_specular;

// Vectors from vertex shader
varying vec4 v_normal;
varying vec4 v_s[MAX_NUMBER_OF_LIGHTS];
//varying vec4 v_h[MAX_NUMBER_OF_LIGHTS];
varying vec4 v_v; //vector towards the camera

// Lets do some fog!
uniform float u_fog_start;
uniform float u_fog_end;
uniform vec4 u_fog_color;

varying float v_distance;

varying vec2 v_uv; // adding the textures
//It updates position way too fast if I use varying variable
//varying vec4 v_light_position;

// FRAGMENT SHADER
void main(void)
{	

	/*=====================================================================================
	** Ambient and Material
	**=====================================================================================*/
	//The ambient light
	vec4 ambient = u_ambient_strenght + u_ambient_diffuse ;

	//Set the initial values
	vec4 mat_diffuse = u_mat.diffuse;
	vec4 mat_specular = u_mat.specular;

	float opacity = u_mat.diffuse.a;

	//Apply the texture to the material when needed
	if (u_using_texture > 0){
		mat_diffuse = u_mat.diffuse * texture2D(u_texture, v_uv) ;
		if (opacity < 1){
			//If material alpha is less than 1,
			//then we use specularity texture to map transparency				
			opacity = 1 - texture2D(u_texture_specular,v_uv).r;
		} else {
			//Else we use specularity texture to map shiny surfaces
			mat_specular *= texture2D(u_texture_specular, v_uv);
		}
	} 

	/*=====================================================================================
	** Calculate the lights
	**=====================================================================================*/
	//We calculate each light individually
	//and accumilate the diffuse and specularity
	vec4 total_light_diffuse = vec4(0);
	vec4 total_light_specular = vec4(0);

	//If the normal is facing away from us, we want to invert the normal (making the surface
	//seem like it has two sides)
	/* 
	vec4 normal;
	if (dot(v_v, v_normal) < 0.0){
		normal = -1 * v_normal;
	} else {
		normal = v_normal;
	}
 	*/
	 
	//It would be better to simply not render what is facing away from us
	if (dot(v_v, v_normal) < 0.0){
		//Just discard the fragment
		discard;
	}


	float n_len = length (v_normal);
	for (int i = 0; i < MAX_NUMBER_OF_LIGHTS ; i++){
		//Smoother lights by dividing the dot product with (n_len*s_len) and (n_len*h_len) respectively
		float s_len = length(v_s[i]);
		//Halway vector of the light reaching our camera
		vec4 h =v_s[i] + v_v;
		float h_len = length(h);

		float curr_lambert = max(dot(v_normal, v_s[i])/ (n_len*s_len),0); //light intensity
		float curr_phong = max(dot(v_normal, h)/(n_len*h_len),0);	//reflective-ness

		total_light_diffuse = total_light_diffuse + u_lights[i].diffuse * curr_lambert;
		total_light_specular = total_light_specular + u_lights[i].specular * pow(curr_phong, u_mat.shinyness);
	}

	//Then use put it together with the materials to get final fragment color
	vec4 final_color =  (total_light_diffuse + ambient)* mat_diffuse//diffuse
			+ mat_specular * (total_light_specular * u_mat.shinyness)  //specular
	; 

	/*=====================================================================================
	** Adding Fog
	**=====================================================================================*/
	if (u_fog > 0){
		if (v_distance < u_fog_start){
			//clear colors
			gl_FragColor = final_color;
		} else if(v_distance > u_fog_end){
			//fog color overwrites frag color
			gl_FragColor = u_fog_color;
		} else {	
			//varies by distance
			float fog_level = (v_distance - u_fog_start) / (u_fog_end - u_fog_start); 
			gl_FragColor = (1 - fog_level)*final_color + (fog_level * u_fog_color);	 
			gl_FragColor.a = u_mat.diffuse.a; //for the alpha value
		}
	} else {
		gl_FragColor = final_color;
	}
	gl_FragColor.a = opacity;
}