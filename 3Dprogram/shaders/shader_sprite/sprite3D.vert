/*
This is the first shader, so it does not work passing varying variables from fragment shader to the vertex shader, that's like throwing information up stream.
*/
//====================== ATTRIBUTE VARIABLES ======================
attribute vec3 a_position;
attribute vec2 a_uv;

//====================== UNIFORM VARIABLES ======================
//Matrices
uniform mat4 u_model_matrix;
uniform mat4 u_view_matrix;
uniform mat4 u_projection_matrix;

//Textures / UV
varying vec2 v_uv;

// VERTEX SHADER
void main(void)
{
	/*=====================================================================================
	** ATTRIBUTES
	**=====================================================================================*/
	vec4 position = vec4(a_position.x, a_position.y, a_position.z, 1.0); //1 means it's a point
	v_uv = a_uv;


	/*---- LOCAL COORDINATES -------------------------------------------------------------*/
	position = u_model_matrix * position;

	/*---- EYE COORDINATES ---------------------------------------------------------------*/
	position = u_view_matrix * position;

	/*---- CLIP COORDINATES --------------------------------------------------------------*/
	position = u_projection_matrix * position;



	/*=====================================================================================
	** FEED POSITION TO OPENGL
	**=====================================================================================*/
	gl_Position = position;
}