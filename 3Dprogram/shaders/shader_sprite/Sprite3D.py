import sys

from OpenGL.GL import *
import OpenGL.GLU
from matrices import *

class SpriteShader:
    def __init__(self):
        vert_shader = sys.path[0] + "/shaders/shader_sprite/sprite3D.vert"
        frag_shader = sys.path[0] + "/shaders/shader_sprite/sprite3D.frag"
        super().__init__(vert_shader, frag_shader)
        #=====================================================================================
        # HANDLES FOR ATTRIBUTES 
        #=====================================================================================
        #----------- POSITION
        self.positionLoc			= glGetAttribLocation(self.renderingProgramID, "a_position") 
        glEnableVertexAttribArray(self.positionLoc)

        #----------- TEXTURES / UV
        self.uvLoc			= glGetAttribLocation(self.renderingProgramID, "a_uv") 
        glEnableVertexAttribArray(self.uvLoc)

        #=====================================================================================
        # HANDLES FOR OTHER VARIABLES
        #=====================================================================================
        #----------- MATRICES
        self.modelMatrixLoc			= glGetUniformLocation(self.renderingProgramID, "u_model_matrix")
        self.projectionMatrixLoc	= glGetUniformLocation(self.renderingProgramID, "u_projection_matrix")
        self.viewMatrixLoc			= glGetUniformLocation(self.renderingProgramID, "u_view_matrix")

        self.diffuseTextureLoc		= glGetUniformLocation(self.renderingProgramID, "u_texture")
        self.alphaTextureLoc        = glGetUniformLocation(self.renderingProgramID, "u_alpha_texture")

        self.usingAlphaTextureLoc   = glGetUniformLocation(self.renderingProgramID, "u_using_alpha_texture")
        self.opacityLoc             = glGetUniformLocation(self.renderingProgramID, "u_opacity")


    #=====================================================================================
    # MATRICES
    #=====================================================================================
    #----------- MODEL MATRIX
    def set_model_matrix(self, matrix_array):
        glUniformMatrix4fv(self.modelMatrixLoc, 1, True, matrix_array)

    #----------- VIEW MATRIX
    def set_view_matrix(self, matrix_array):
        glUniformMatrix4fv(self.viewMatrixLoc, 1, True, matrix_array)

    #----------- PROJECTION MATRIX
    def set_projection_matrix(self, matrix_array):
        glUniformMatrix4fv(self.projectionMatrixLoc, 1, True, matrix_array)


    #=====================================================================================
    # ATTRIBUTES 
    #=====================================================================================
    #-------- POSITION 
    def set_position_attribute(self, vertex_array):
        """Tells the shader where all the vertices are to draw the polygons"""
        glVertexAttribPointer(self.positionLoc, 3, GL_FLOAT, False, 0, vertex_array)

    #-------- TEXTURE / UV 
    def set_uv_attribute(self, vertex_array):
        """Tells the shader how to map the texture onto each vertex"""
        glVertexAttribPointer(self.uvLoc, 2, GL_FLOAT, False, 0, vertex_array)

    #----------- BUFFERS
    def set_attribute_buffers_with_uv(self, vertex_buffer_id:int):
        """Yet another version to input the vertices, this time we include the normals in the same array
        \nadds uv coordinates ontop of the set_attribute_buffers_with_normals"""
        glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_id)
        # 6* sizeof(GLfloat) is the stride, so it will take steps of 6 numbers, then read 3, then some GLU ctypes void pointers
        glVertexAttribPointer(self.positionLoc, 3, GL_FLOAT, False, 8* sizeof(GLfloat), OpenGL.GLU.ctypes.c_void_p(0))
        glVertexAttribPointer(self.uvLoc, 2, GL_FLOAT, False, 8* sizeof(GLfloat), OpenGL.GLU.ctypes.c_void_p(6 * sizeof(GLfloat)))

    #=====================================================================================
    # UNIFORM VARIABLES 
    #=====================================================================================

    def set_diffuse_texture(self):
        """Tell the shader to use GL_TEXTURE1 for diffuse"""
        glUniform1i(self.diffuseTextureLoc,0)

    def set_alpha_texture(self, texture:int):
        glUniform1i(self.usingAlphaTextureLoc,1)
        glUniform1i(self.alphaTextureLoc,texture)