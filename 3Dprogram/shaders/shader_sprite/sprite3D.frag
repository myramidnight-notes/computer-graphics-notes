

//For the textures
uniform sampler2D u_texture;
uniform sampler2D u_alpha_texture;
uniform int u_using_alpha_texture;

uniform float u_opacity;


varying vec2 v_uv; // adding the textures

void main(void)
{	

	/*=====================================================================================
	** Ambient and Material
	**=====================================================================================*/
	//There is just one ambient, no need to loop that
	vec4 color = texture2D(u_texture,v_uv);
    float opacity = u_opacity;

	//Apply the texture to the material when needed
	if (u_using_alpha_texture > 0){
		opacity *= texture2D(u_alpha_texture, v_uv).r;
	} 

    if (opacity < 0.01){
        discard;
    }
	/*=====================================================================================
	** Calculate the lights
	**=====================================================================================*/
	//We calculate each light individually
	//and accumilate the diffuse and specularity
    gl_FragColor = color;
    gl_FragColor.a = opacity;
}