

uniform samplerCube u_skybox_texture;
varying vec4 v_texture_position;
void main(){
    gl_FragColor = texture(u_skybox_texture, v_texture_position);
}