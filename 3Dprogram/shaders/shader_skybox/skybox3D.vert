uniform mat4 u_model_matrix;
uniform mat4 u_view_matrix;
uniform mat4 u_projection_matrix;

varying vec3 v_texture_position;

void main(){   

    vec4 position = vec4(a_position.x, a_position.y, a_position.z, 1.0);
    v_texture_position = vec3(u_model_matrix * position);
    gl_Position = pos.xyww;   //shoving the box into the back of the z buffer
}