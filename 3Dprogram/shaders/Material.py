from typing import TYPE_CHECKING

from .Color import Color


class Material:
    def __init__(self, diffuse:'Color' = Color(1,1,1), specular:'Color' = Color(0,0,0), shiny:float = 1):
        """Just to hold the information about a material,
        \nit's diffuse, specularity and shinyness"""

        self.specular: 'Color' = specular
        self.diffuse: 'Color' = diffuse
        self.shininess: float = shiny

        self.has_texture:bool = False

        self.texture_diffuse:int = None
        self.texture_specular:int = None
        self.texture_alpha:int = None

    def set_textures(self,diffuse:int = None, specular:int = None, alpha:int = None):
        """Sets the textures for this specific material, default is None
        \nYou can set the diffuse texture, specular texture, and alpha texture.
        \nIt will take in the texture ID (a integer)"""
        self.texture_diffuse = diffuse
        self.texture_specular = specular
        self.texture_alpha = alpha
        self.has_texture = True

    def __str__(self):
        return "Material(diffuse = Color({},{},{},{}), specular = Color({},{},{},{})), shininess = {})".format(self.diffuse.r,self.diffuse.g,self.diffuse.b,self.diffuse.a,self.specular.r,self.specular.g,self.specular.b,self.specular.a, self.shininess)

# Just to see how complex I had it before...
class OldMaterial:
    def __init__(self,rDiffuse:float=1,gDiffuse:float=1,bDiffuse:float=1, rSpecular:float=1,gSpecular:float=1,bSpecular:float=1, shiny:float = 0):
        """Just to hold the information about a material,
        \nit's diffuse, specularity and shinyness"""

        self._rSpecular:float = rSpecular
        self._gSpecular:float = gSpecular
        self._bSpecular:float = bSpecular

        self._rDiffuse:float = rDiffuse
        self._gDiffuse:float = gDiffuse
        self._bDiffuse:float = bDiffuse

        self._shiny:float = shiny

    def set_diffuse(self,  red:float,green:float,blue:float):
        """To update the diffuse"""
        self._rDiffuse = red
        self._gDiffuse = green
        self._bDiffuse = blue


    def set_specular(self, red:float,green:float,blue:float):
        """update the specularity"""
        self._rSpecular = red
        self._gSpecular = green
        self._bSpecular = blue

    def set_shiny(self,shiny:float):
        self._shiny = shiny