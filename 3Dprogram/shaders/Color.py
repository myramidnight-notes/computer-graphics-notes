#=====================================================================================
# COLOR
#=====================================================================================
class Color:
    """Color class (just uses rgb instead of xyz)"""
    def __init__(self, r:float, g:float, b:float, a:float = 1):
        self.r:float = r
        self.g:float = g
        self.b:float = b
        self.a:float = a

    def __add__(self, other):
        return Color(self.r + other.r, self.g + other.g, self.b + self.a * other.a)

    def __sub__(self, other):
        return Color(self.r - other.r, self.g - other.g, self.b - other.b, self.a * other.a)

    def __str__(self):
        return "Color({},{},{},{})".format(self.r, self.g, self.b, self.a)