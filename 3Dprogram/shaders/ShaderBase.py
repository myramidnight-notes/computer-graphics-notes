from abc import ABC
from OpenGL.GL import *
import OpenGL.GLU
from matrices import *

class ShaderBase(ABC):
    """Reusable base for the various shaders, since they all start the same"""
    def __init__(self, vert_shader_path:str, frag_shader_path:str):
        """Provide the path to the vertex and fragment shaders"""
        vert_shader = glCreateShader(GL_VERTEX_SHADER)
        shader_file = open(vert_shader_path)
        glShaderSource(vert_shader,shader_file.read())
        shader_file.close()
        glCompileShader(vert_shader)
        result = glGetShaderiv(vert_shader, GL_COMPILE_STATUS)
        if (result != 1): # shader didn't compile
            print("Couldn't compile vertex shader\nShader compilation Log:\n" + str(glGetShaderInfoLog(vert_shader)))

        frag_shader = glCreateShader(GL_FRAGMENT_SHADER)
        shader_file = open(frag_shader_path)
        glShaderSource(frag_shader,shader_file.read())
        shader_file.close()
        glCompileShader(frag_shader)
        result = glGetShaderiv(frag_shader, GL_COMPILE_STATUS)
        if (result != 1): # shader didn't compile
            print("Couldn't compile fragment shader\nShader compilation Log:\n" + str(glGetShaderInfoLog(frag_shader)))

        self.renderingProgramID = glCreateProgram()
        glAttachShader(self.renderingProgramID, vert_shader)
        glAttachShader(self.renderingProgramID, frag_shader)
        glLinkProgram(self.renderingProgramID)

    #=====================================================================================
    # USE RENDERING PROGRAM
    #=====================================================================================
    def use(self):
        """Can't use the shader without using it"""
        try:
            glUseProgram(self.renderingProgramID)   
        except OpenGL.error.GLError:
            print(glGetProgramInfoLog(self.renderingProgramID))
            raise

