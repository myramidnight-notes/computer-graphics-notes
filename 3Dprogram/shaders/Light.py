from matrices import Point
from shaders import Color

class Store(object):
    pass

class Light:
    def __init__(self,pos:Point, color:Color , specular: Color):
        """Just to hold the information about a light, position is required
        \ndefault is white light diffuse and no specularity.
        \nuse set_diffuse and set_specular to change defaults"""

        self.position:Point = pos
        self.diffuse:Color = color
        self.specular:Color = specular

        # So we can have a directional light
        self.dir_light : bool = False
        self._direction:Point = Point(0,0,0)

    def set_directional_light(self, direction:Point):
        """To change this into a directional light"""
        self.dir_light:bool = True
        self._direction:Point = direction

    def __str__(self):
        return "Light(position = ({},{},{}), diffuse = Color({},{},{},{}), specular = Color({},{},{},{}))".format(self.position.x,self.position.y,self.position.z,self.diffuse.r,self.diffuse.g,self.diffuse.b,self.diffuse.a,self.specular.r,self.specular.g,self.specular.b,self.specular.a)