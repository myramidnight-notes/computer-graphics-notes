from . import Vector,Point
from math import cos, sin

# The ViewMatrix class holds the camera's coordinate frame and
# set's up a transformation concerning the camera's position
# and orientation

class ViewMatrix:
    """The Camera Matrix (where it is, and where it's aimed, where is up)"""
    def __init__(self):
        self.eye = Point(0, 0, 0)
        self.u = Vector(1, 0, 0) # the x of camera
        self.v = Vector(0, 1, 0) # the y of camera
        self.n = Vector(0, 0, 1) # vector out of back of camera
        self.grounded = False

    #=====================================================================================
    # SETTINGS / SETUP
    #=====================================================================================

    def setGrounded(self, status:bool) -> None:
        """Decides if camera is grounded, or can fly through space, changes behaviour of some of the methods"""
        self.grounded = status

    def look(self, eye:Point, center:Point, up:Vector) -> None:
        """Sets where the camera is looking: look(eye,center,up) 
        \n\teye = where the camera should be, 
        \n\tcenter = what is the camera is looking at, 
        \n\tup = which direction is up"""
        self.eye = eye
        self.n = eye - center
        self.n.normalize()
        self.u = up.cross(self.n)
        self.u.normalize()
        self.v = self.n.cross(self.u)

    #=====================================================================================
    # SLIDE
    #=====================================================================================

    def slide(self, del_u:float = 0, del_v:float =0, del_n:float = 0) -> None:
        """Relative movement to the camera. Movement without rotation, relative to camera axis"""
        new_u = self.u * del_u
        new_v = self.v * del_v
        new_n = self.n * del_n

        #To prevent us from lifting off the ground if camera is grounded
        #TODO have this be decided by the 'up' vertex, rather than hardcoded to y-axis
        if (self.grounded):
            new_n.y = self.eye.y
            new_u.y = self.eye.y
            self.eye += new_u + new_n
        else:
            self.eye += new_u + new_n + new_v


    #=====================================================================================
    # SPIN: ROLL, YAW, PITCH
    #=====================================================================================
    #TODO prevent grounded camera from rolling when changing pitch

    def __spin(self, angle, dimension:str = "n") -> None:
        """Spins the matrice around an axis"""
        c = cos(angle)
        s = sin(angle)
        
        if dimension == "n":
            temp_u = self.u * c + self.v * s
            self.v = self.u * -s + self.v * c
            self.u = temp_u
        elif dimension == "u":
            temp_v = self.v * c + self.n * s
            self.n = self.v * -s + self.n * c
            self.v = temp_v
        elif dimension == "v":
            temp_n = self.n * c + self.u * s
            self.u = self.n * -s + self.u * c 
            self.n = temp_n
        
    def roll(self, angle) -> None:
        """'Do a barrel ROll! Spins the camera around the n axis"""
        self.__spin(angle,"n")

    def yaw(self,angle) -> None:
        """Spins the camera around the v axis (y axis)"""
        self.__spin(angle, "v")

    def pitch(self, angle) -> None:
        """Spins the camera around the u axis (x axis)"""
        self.__spin(angle, "u")





    #=====================================================================================
    # MATRIX
    #=====================================================================================
    def get_matrix(self) -> list[float]:
        """Returns the view matrix array"""
        minusEye = Vector(-self.eye.x, -self.eye.y, -self.eye.z)
        return [self.u.x, self.u.y, self.u.z, minusEye.dot(self.u),
                self.v.x, self.v.y, self.v.z, minusEye.dot(self.v),
                self.n.x, self.n.y, self.n.z, minusEye.dot(self.n),
                0,        0,        0,        1]

