
from math import * # trigonometry
from typing import TYPE_CHECKING
from matrices import Vector

if TYPE_CHECKING:
    from matrices.view_matrix import ViewMatrix

class ModelMatrix:
    """Our own matrix setup, That keeps track of where things are in space"""
    def __init__(self):
        self.matrix = [ 1, 0, 0, 0,
                        0, 1, 0, 0,
                        0, 0, 1, 0,
                        0, 0, 0, 1 ]
        self.stack = []
        self.stack_count = 0
        self.stack_capacity = 0

    def get_x(self):
        """"""
        return Vector(self.matrix[0],self.matrix[1],self.matrix[2])

    def get_y(self):
        return Vector(self.matrix[4],self.matrix[5],self.matrix[6])
    
    def get_z(self):
        return Vector(self.matrix[8],self.matrix[9],self.matrix[10])

    def get_center(self):
        return Vector(self.matrix[12],self.matrix[13],self.matrix[14])

    #=====================================================================================
    # LOAD MATRIX
    #=====================================================================================
    def load_identity(self):
        self.matrix = [ 1, 0, 0, 0,
                        0, 1, 0, 0,
                        0, 0, 1, 0,
                        0, 0, 0, 1 ]

    #=====================================================================================
    # COPY MATRIX
    #=====================================================================================
    def copy_matrix(self):
        """Returns a duplicate of current matrix"""
        new_matrix = [0] * 16
        for i in range(16):
            new_matrix[i] = self.matrix[i]
        return new_matrix

    #=====================================================================================
    # TRANDFORMATION
    #=====================================================================================
    def add_transformation(self, matrix2):
        """Transforms the matrix based on given matrix"""
        counter = 0
        new_matrix = [0] * 16
        for row in range(4):
            for col in range(4):
                for i in range(4):
                    new_matrix[counter] += self.matrix[row*4 + i]*matrix2[col + 4*i]
                counter += 1
        self.matrix = new_matrix

    #=====================================================================================
    # TRANSLATION
    #=====================================================================================
    def add_translation(self,x:float = 0,y:float = 0,z:float = 0):
        """Translates the matrix according to a given axis, they default as 0"""
        other_matrix = [1, 0, 0, x,
                        0, 1, 0, y,
                        0, 0, 1, z,
                        0, 0, 0, 1]
        self.add_transformation(other_matrix)

    #=====================================================================================
    # SCALE
    #=====================================================================================
    def add_scale(self,x:float=1,y:float=1,z:float=1):
        """Scales the matrix according to given axis, defaults to 1"""
        other_matrix = [x, 0, 0, 0,
                        0, y, 0, 0,
                        0, 0, z, 0,
                        0, 0, 0, 1]
        self.add_transformation(other_matrix)

    #=====================================================================================
    # ROTATION
    #=====================================================================================
    def add_rotation_x(self,angle):
        """Rotate the matrix around the x axis, by given angle"""
        c = cos(angle)
        s = sin(angle)
        other_matrix = [1, 0, 0, 0,
                        0, c, -s, 0,
                        0, s, c, 0,
                        0, 0, 0, 1]
        self.add_transformation(other_matrix)

    def add_rotation_y(self,angle):
        """Rotate the matrix around the y axis, by given angle"""
        c = cos(angle)
        s = sin(angle)
        other_matrix = [c, 0, s, 0,
                        0, 1, 0, 0,
                        -s, 0, c, 0,
                        0, 0, 0, 1]
        self.add_transformation(other_matrix)

    def add_rotation_z(self,angle):
        """Rotate the matrix around the z axis, by given angle"""
        c = cos(angle)
        s = sin(angle)
        other_matrix = [c, -s, 0, 0,
                        s, c, 0, 0,
                        0, 0, 1, 0,
                        0, 0, 0, 1]
        self.add_transformation(other_matrix)



    #=====================================================================================
    # PUSH / POP MATRIX
    #=====================================================================================
    # YOU CAN TRY TO MAKE PUSH AND POP (AND COPY) LESS DEPENDANT ON GARBAGE COLLECTION
    # THAT CAN FIX SMOOTHNESS ISSUES ON SOME COMPUTERS
    def push_matrix(self):
        """Same as 'glPushMatrix', just we manage our own matrices """
        self.stack.append(self.copy_matrix())

    def pop_matrix(self):
        """Same as 'glPopMatrix', just we manage our own matrices """
        self.matrix = self.stack.pop()

    #=====================================================================================
    # DEBUGGING / PRINT MATRIX
    #=====================================================================================
    # This operation mainly for debugging
    def __str__(self):
        ret_str = ""
        counter = 0
        for _ in range(4):
            ret_str += "["
            for _ in range(4):
                ret_str += " " + str(self.matrix[counter]) + " "
                counter += 1
            ret_str += "]\n"
        return ret_str

