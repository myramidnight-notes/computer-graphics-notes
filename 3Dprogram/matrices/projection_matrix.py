from math import *
# The ProjectionMatrix class builds transformations concerning
# the camera's "lens"
class ProjectionMatrix:
    """How things look to the camera (dept, perspective..)"""
    def __init__(self):
        self.left = -1
        self.right = 1
        self.bottom = -1
        self.top = 1
        self.near = -1
        self.far = 1

        self.is_orthographic = True

    #=====================================================================================
    # PERSPECTIVE
    #=====================================================================================
    def set_perspective(self, fov, aspect, near, far):
        """set_persective(fov, aspect, near, far)
        \n\t fov = field of view"""
        self.near = near
        self.far = far
        self.top = near * tan(fov / 2)
        self.bottom = -self.top
        self.right = self.top * aspect #how much wider it is than hight
        self.left = -self.right
        self.is_orthographic = False

    #=====================================================================================
    # ORTHOGRAPHIC 
    #=====================================================================================
    def set_orthographic(self, left, right, bottom, top, near, far):
        self.left = left
        self.right = right
        self.bottom = bottom
        self.top = top
        self.near = near
        self.far = far
        self.is_orthographic = True

    #=====================================================================================
    # MATRIX
    #=====================================================================================
    def get_matrix(self):
        if self.is_orthographic:
            A = 2 / (self.right - self.left)
            B = -(self.right + self.left) / (self.right - self.left)
            C = 2 / (self.top - self.bottom)
            D = -(self.top + self.bottom) / (self.top - self.bottom)
            E = 2 / (self.near - self.far)
            F = (self.near + self.far) / (self.near - self.far)

            return [A,0,0,B,
                    0,C,0,D,
                    0,0,E,F,
                    0,0,0,1]

        else:
            #TODO Set up a matrix for a Perspective projection
            ###  Remember that it's a non-linear transformation   ###
            ###  so the bottom row is different                   ###
            A = (2 * self.near)/(self.right - self.left)
            B = (self.right + self.left) / (self.right-self.left)
            C = (2 * self.near)/(self.top - self.bottom)
            D = (self.top + self.bottom) / (self.top - self.bottom)
            E = -(self.far + self.near)/(self.far - self.near)
            F = (-2 - self.far * self.near)/(self.far - self.near)

            return [A,0,B,0,
                    0,C,D,0,
                    0,0,E,F,
                    0,0,-1,0]
