
from random import *
from pygame import Vector3

from OpenGL.GL import *
from OpenGL.GLU import *

from math import *


#=====================================================================================
# POINT
#=====================================================================================
class Point:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def __add__(self, other):
        return Point(self.x + other.x, self.y + other.y, self.z + other.z)

    def __sub__(self, other):
        return Vector(self.x - other.x, self.y - other.y, self.z - other.z)

#=====================================================================================
# VECTOR
#=====================================================================================
class Vector:
    """Our custom vector class"""
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
    
    def __add__(self, other):
        return Vector(self.x + other.x, self.y + other.y, self.z + other.z)

    def __sub__(self, other):
        return Vector(self.x - other.x, self.y - other.y, self.z - other.z)

    def __mul__(self, scalar):
        return Vector(self.x * scalar, self.y * scalar, self.z * scalar)
    
    def __len__(self):
        return sqrt(self.x * self.x + self.y * self.y + self.z * self.z)
    
    def normalize(self):
        length = self.__len__()
        self.x /= length
        self.y /= length
        self.z /= length

    def dot(self, other):
        """Dot product between this and other vector"""
        return self.x * other.x + self.y * other.y + self.z * other.z

    def cross(self, other):
        """Cross product between this and other vector,. 
        \n a perpendicular vector from either vector"""
        return Vector(self.y*other.z - self.z*other.y, self.z*other.x - self.x*other.z, self.x*other.y - self.y*other.x)

    def length(self) -> int:
        """Also just called magnitute of the vector"""
        return sqrt(self.x * self.x + self.y * self.y + self.z * self.z)

    def get_angle(self, other) -> float:
        this_v = Vector3(self.x, self.y, self.z)
        other_v = Vector3(other.x, other.y, other.z)
        return this_v.angle_to(other_v)
