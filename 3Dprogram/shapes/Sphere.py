from math import pi, sin, cos

import numpy
from OpenGL.GL import *
from OpenGL.GLU import *
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from shaders import Shader3D

class ArraySphere:
    def __init__(self, stacks = 12, slices = 24):
        self.vertex_array = []
        self.slices = slices

        self.vertex_count = 0
        self.__generateSphere(stacks, slices)

    def __generateSphere(self, stacks, slices):
        """Creates the vertex array"""
        stack_interval = pi / stacks
        slice_interval = 2.0 * pi /slices
        for stack_count in range(stacks):
            stack_angle = stack_count * stack_interval
            for slice_count in range(slices + 1):
                slice_angle = slice_count * slice_interval
                self.vertex_array.append(sin(stack_angle) * cos(slice_angle))
                self.vertex_array.append(cos(stack_angle))
                self.vertex_array.append(sin(stack_angle)* sin(slice_angle))   

                self.vertex_array.append(sin(stack_angle + stack_interval) * cos(slice_angle))
                self.vertex_array.append(cos(stack_angle + stack_interval))
                self.vertex_array.append(sin(stack_angle + stack_interval) * sin(slice_angle))

                self.vertex_count += 2

    def setSphere(self, stacks, slices):
        """Re-initiates the sphere"""
        self.vertex_count = 0
        self.vertex_array = []
        self.slices = slices
        self.__generateSphere(stacks, slices)

    def __setVertices(self, shader):
        """Gives the shader the vertices for the sphere"""
        shader.set_position_attribute(self.vertex_array)
        shader.set_normal_attribute(self.vertex_array)

    def draw(self, shader):
        """Draws the sphere"""
        self.__setVertices(shader)
        for i in range(0, self.vertex_count, (self.slices + 1) * 2):
            glDrawArrays(GL_TRIANGLE_STRIP, i, (self.slices + 1) * 2)
            
class Sphere:
    """Trying to make it use vertex buffers"""
    def __init__(self, stacks = 12, slices = 24):
        self.vertex_array = []
        self.slices = slices

        self.vertex_count = 0
        #generate the vertices
        self.__generateSphere(stacks, slices)

        # Making a buffer, so we don't have to use setVertices every single time
        self.vertex_buffer_id = glGenBuffers(1) #how many buffers do we want?
        glBindBuffer(GL_ARRAY_BUFFER, self.vertex_buffer_id) #bind the buffer

        # Fill the buffer
        glBufferData(GL_ARRAY_BUFFER, numpy.array(self.vertex_array,dtype='float32' ),GL_STATIC_DRAW)
        glBindBuffer(GL_ARRAY_BUFFER, 0) #unbinding GL_ARRAY_BUFFER, so other models can use it too
        self.vertex_array = None #clearing the space, since we got it in a buffer now

    def __generateSphere(self, stacks, slices):
        """Creates the vertex array"""
        stack_interval = pi / stacks
        slice_interval = 2.0 * pi /slices

        for stack_count in range(stacks):
            stack_angle = stack_count * stack_interval
            for slice_count in range(slices + 1):
                slice_angle = slice_count * slice_interval
                #doubles the array, to include the normals (simple, since sphere uses same list for both positions and normals)
                for _ in range(2):
                    self.vertex_array.append(sin(stack_angle) * cos(slice_angle))
                    self.vertex_array.append(cos(stack_angle))
                    self.vertex_array.append(sin(stack_angle)* sin(slice_angle))   

                #dadding the texture 2D mapping to the array as well
                self.vertex_array.append(slice_count / slices)
                self.vertex_array.append(stack_count / stacks)
                for _ in range(2):
                    self.vertex_array.append(sin(stack_angle + stack_interval) * cos(slice_angle))
                    self.vertex_array.append(cos(stack_angle + stack_interval))
                    self.vertex_array.append(sin(stack_angle + stack_interval) * sin(slice_angle))

                self.vertex_array.append(slice_count / slices)
                self.vertex_array.append((stack_count+1) / stacks)

                self.vertex_count += 2

    def draw(self, shader:'Shader3D'):
        """Draws the sphere"""
        shader.set_attribute_buffers_with_uv(self.vertex_buffer_id)
        for i in range(0, self.vertex_count, (self.slices + 1) * 2):
            glDrawArrays(GL_TRIANGLE_STRIP, i, (self.slices + 1) * 2)
        glBindBuffer(GL_ARRAY_BUFFER, 0) #unbinding buffer after drawing

            