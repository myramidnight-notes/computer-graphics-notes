
from OpenGL.GL import *
from OpenGL.GLU import *
from typing import TYPE_CHECKING, List
from numpy import array

if TYPE_CHECKING:
    from shaders import Shader3D

class Floor:
    def __init__(self):
        # Every 3 numbers are a point, 
        # Each surface is a group of 4 points
        self.position_array = [
                            -0.5, 0, -0.5,
                            0.5, 0, -0.5,
                            0.5, 0, 0.5,
                            -0.5, 0, 0.5]

        self.normal_array = [0.0, 1.0, 0.0] * 4
        
        self.uv_array = [
            0.0, 0.0,
            0.0, 1.0,
            1.0, 1.0,
            1.0, 0.0] 

    def set_vertices(self, shader):
        shader.set_position_attribute(self.position_array)
        shader.set_normal_attribute(self.normal_array)
        shader.set_uv_attribute(self.uv_array) #This seems to make light not effect our cubes anymore

    def draw(self,shader):
        """Draws a cube, each face is a triangle_fan""" 
        self.set_vertices(shader)
        #Drawing each side
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4) #front  