from OpenGL.GL import *
from OpenGL.GLU import *

class Skybox:
    def __init__(self):
        # Every 3 numbers are a point, 
        # Each surface is a group of 4 points
        self.position_array = [-0.5, -0.5, -0.5,
                            -0.5, 0.5, -0.5,
                            0.5, 0.5, -0.5,
                            0.5, -0.5, -0.5,
                            -0.5, -0.5, 0.5,
                            -0.5, 0.5, 0.5,
                            0.5, 0.5, 0.5,
                            0.5, -0.5, 0.5,
                            -0.5, -0.5, -0.5,
                            0.5, -0.5, -0.5,
                            0.5, -0.5, 0.5,
                            -0.5, -0.5, 0.5,
                            -0.5, 0.5, -0.5,
                            0.5, 0.5, -0.5,
                            0.5, 0.5, 0.5,
                            -0.5, 0.5, 0.5,
                            -0.5, -0.5, -0.5,
                            -0.5, -0.5, 0.5,
                            -0.5, 0.5, 0.5,
                            -0.5, 0.5, -0.5,
                            0.5, -0.5, -0.5,
                            0.5, -0.5, 0.5,
                            0.5, 0.5, 0.5,
                            0.5, 0.5, -0.5]

        normal_back = [0.0, 0.0, -1.0]
        normal_front = [0.0, 0.0, 1.0]
        normal_bottom = [0.0, -1.0, 0.0]
        normal_top = [0.0, 1.0, 0.0]
        normal_left = [-1.0, 0.0, 0.0]
        normal_right = [1.0, 0.0, 0.0]
        self.normal_array = normal_back * 4 + normal_front * 4 + normal_bottom * 4 + normal_top * 4 + normal_left * 4 + normal_right * 4
        
        self.uv_array = [
            0.0, 0.0,
            0.0, 1.0,
            1.0, 1.0,
            1.0, 0.0] * 6

    def set_vertices(self, shader):
        shader.set_position_attribute(self.position_array)
        shader.set_normal_attribute(self.normal_array)
        shader.set_uv_attribute(self.uv_array) #This seems to make light not effect our cubes anymore

    def draw(self,shader):
        """Draws a cube, each face is a triangle_fan"""
        self.set_vertices(shader)
        #Drawing each side
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4) #front  
        glDrawArrays(GL_TRIANGLE_FAN, 4, 4) #back  
        glDrawArrays(GL_TRIANGLE_FAN, 8, 4) #bottom
        glDrawArrays(GL_TRIANGLE_FAN, 12, 4) #top
        glDrawArrays(GL_TRIANGLE_FAN, 16, 4) #left
        glDrawArrays(GL_TRIANGLE_FAN, 20, 4) #Right
