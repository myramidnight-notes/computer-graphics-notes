from OpenGL.GL import *
from OpenGL.GLU import *
import numpy
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from shaders import SpriteShader

class Sprite:
    def __init__(self, height:float = 1, width:float = 1):
        """A sprite is rendered so that bottom middle is (0,0)"""
        bot_edge   = 0
        top_edge   = height
        left_edge  = -(width/2)
        right_edge = (width/2)

        position_array = [
            left_edge, bot_edge, 0,0.0, 0.0,
            left_edge, top_edge, 0,0.0, 1.0,
            right_edge, top_edge, 0,1.0, 1.0,
            right_edge, bot_edge, 0, 1.0, 0.0
        ]

        #create the buffer
        self.buffer_id = glGenBuffers(1)
        glBindBuffer(GL_ARRAY_BUFFER, self.buffer_id)
        glBufferData(GL_ARRAY_BUFFER, numpy.array(position_array, dtype='float32'),GL_STATIC_DRAW)
        glBindBuffer(GL_ARRAY_BUFFER,0)
        
        position_array = None

    def setVertices(self, shader:'SpriteShader'):
        """Draw the sprite using the sprite shader"""
        shader.set_attribute_buffers_with_uv(self.buffer_id)
        glDrawArrays(GL_TRIANGLE_FAN, 0,4)
        glBindBuffer(GL_ARRAY_BUFFER, 0)

    def draw(self,shader):
        """Draws a cube, each face is a triangle_fan"""
        self.setVertices(shader)
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4) #back  