from abc import ABC, abstractmethod
from OpenGL.GL import *
from OpenGL.GLU import *

# I kind of took this cube appart into molicules, to figure out what point was what, 
# and then took it a step further: reusable cube with definable edges

class CubeBase(ABC):
    """Customizable CUBE! You can use this to generate cubes of any sort
    \nspecifying what sides should be included, where the edges should be  (rezising).
    \nit will generate the vertex arrays, only including the sides you wanted.
    \nREMEMBER to use the initiateCube() method in the __init__ of any child shape"""
    def __init__(self):
        self.left_edge  = -0.5
        self.right_edge =  0.5
        self.back_edge  = -0.5
        self.front_edge =  0.5
        self.top_edge   =  0.5
        self.bot_edge   = -0.5

        self.position_array = []
        self.normal_array = []
        self.uv_array = []
        
        #List of all possible sides
        self.all_sides = ['back','front','bottom','top','left','right']

        #list of sides that will be used when initateCube is run
        self.sides = [] 

        #just a check to be sure cube has been initiated to prevent breaking things
        self.initiated:bool = False

    def setCubeEdges(self, height:float = 1, width:float = 1, depth:float = 1):
        """Resizes the cube, still centered around (0,0), default values are 1
        \nShould be done before cube is initiated"""
        self.back_edge  = -(depth/2)
        self.front_edge = (depth/2)
        self.bot_edge   = -(height/2)
        self.top_edge   = (height/2)
        self.left_edge  = -(width/2)
        self.right_edge = (width/2)

    def initiateCube(self, back:bool = True, front:bool = True, bottom:bool = True, top:bool = True, left:bool = True, right:bool = True):
        """Initiates the cube with current edges, can specify what sides are included, default will render all sides of the cube
        \nrun 'setCubeEdges' first if you want to change the edge positions"""

        # Generate list of edges that will be included
        sides = []
        for side in self.all_sides:
            sides.append(side)

        self.sides = sides
        self.__generatePositions()
        self.__generateUV()
        self.__generateNormals()
        self.initiated = True

    def __generateNormals(self):
        """Generates the normals array"""

        backNormal      = [0.0, 0.0, -1.0]
        frontNormal     = [0.0, 0.0, 1.0]
        bottomNormal    = [0.0, -1.0, 0.0]
        topNormal       = [0.0, 1.0, 0.0]
        leftNormal      = [-1.0, 0.0, 0.0]
        rightNormal     = [1.0, 0.0, 0.0]

        sideNormals = {
            'front':frontNormal,
            'back':backNormal,
            'top':topNormal,
            'bottom':bottomNormal,
            'left':leftNormal,
            'right':rightNormal
        }

        # compose the normals array
        normalArray = []
        for side in self.sides:
            normalArray += sideNormals[side] * 4

        self.normal_array = normalArray

    #TODO have this as a abstract method, each child should have the option to customize how the texture applies
    def __generateUV(self):
        base_uv = [
            0.0, 0.0,
            0.0, 1.0,
            1.0, 1.0,
            1.0, 0.0]

        # Currently it just applies the single texture to all sides
        self.uv_array = base_uv * len(self.sides)

    def __generatePositions(self):
        """Generates the vertices for each side, combining it as a positions array"""

        backSide = [
            self.left_edge, self.bot_edge, self.back_edge,
            self.left_edge, self.top_edge, self.back_edge,
            self.right_edge, self.top_edge, self.back_edge,
            self.right_edge, self.bot_edge, self.back_edge]
        frontSide = [
            self.left_edge, self.bot_edge, self.front_edge,
            self.left_edge, self.top_edge, self.front_edge,
            self.right_edge, self.top_edge, self.front_edge,
            self.right_edge, self.bot_edge, self.front_edge]
        bottomSide = [
            self.left_edge, self.bot_edge, self.back_edge,
            self.right_edge, self.bot_edge, self.back_edge,
            self.right_edge, self.bot_edge,self.front_edge,
            self.left_edge, self.bot_edge, self.front_edge]
        topSide = [
            self.left_edge, self.top_edge, self.back_edge,
            self.right_edge, self.top_edge, self.back_edge,
            self.right_edge, self.top_edge,self.front_edge,
            self.left_edge, self.top_edge, self.front_edge]
        leftSide = [
            self.left_edge, self.bot_edge, self.back_edge,
            self.left_edge, self.bot_edge, self.front_edge,
            self.left_edge, self.top_edge, self.front_edge,
            self.left_edge, self.top_edge, self.back_edge]
        rightSide = [
            self.right_edge, self.bot_edge, self.back_edge,
            self.right_edge, self.bot_edge, self.front_edge,
            self.right_edge, self.top_edge, self.front_edge,
            self.right_edge, self.top_edge, self.back_edge]

        sidePositions = {
            'front': frontSide,
            'back':backSide,
            'top':topSide,
            'bottom':bottomSide,
            'left':leftSide,
            'right':rightSide
        }

        # Compose the positional array
        positionArray = []
        for side in self.sides:
            positionArray += sidePositions[side]

        self.position_array =  backSide+frontSide+bottomSide+topSide+leftSide+rightSide

    def setVertices(self, shader):
        shader.set_position_attribute(self.position_array)
        shader.set_normal_attribute(self.normal_array)
        shader.set_uv_attribute(self.uv_array)

    @abstractmethod #child classes should define this themselves, to skip all the if statements
    def draw(self, shader):
        """Draws a cube, each face is a triangle_fan"""
        #The returns are so we're not trying to render more sides than defined 
        if not self.initiated:
            #initiate it with the current information
            self.initiateCube()

        self.setVertices(shader)
        #Drawing each side
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4)
        if len(self.sides) == 1:
            return
        glDrawArrays(GL_TRIANGLE_FAN, 4, 4)
        if len(self.sides) == 2:
            return
        glDrawArrays(GL_TRIANGLE_FAN, 8, 4)
        if len(self.sides) == 3:
            return
        glDrawArrays(GL_TRIANGLE_FAN, 12, 4)
        if len(self.sides) == 4:
            return
        glDrawArrays(GL_TRIANGLE_FAN, 16, 4)
        if len(self.sides) == 5:
            return
        glDrawArrays(GL_TRIANGLE_FAN, 20, 4)

