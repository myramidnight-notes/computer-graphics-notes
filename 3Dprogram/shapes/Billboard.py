from abc import ABC, abstractmethod
from OpenGL.GL import *
from OpenGL.GLU import *
from shapes.CubeBase import CubeBase

class Billboard(CubeBase):
    def __init__(self, height:float = 1, width:float = 1):
        """A sprite is rendered so that bottom middle is (0,0)"""
        bot_edge   = 0
        top_edge   = height
        left_edge  = -(width/2)
        right_edge = (width/2)

        self.position_array = [
            left_edge, bot_edge, 0,
            left_edge, top_edge, 0,
            right_edge, top_edge, 0,
            right_edge, bot_edge, 0
        ]
        self.uv_array = [
            0.0, 0.0,
            0.0, 1.0,
            1.0, 1.0,
            1.0, 0.0]


        self.normal_array = [0.0, 0.0, 1.0]*4

    def setVertices(self, shader):
        shader.set_position_attribute(self.position_array)
        shader.set_normal_attribute(self.normal_array)
        shader.set_uv_attribute(self.uv_array)

    def draw(self,shader):
        """Draws a cube, each face is a triangle_fan"""
        self.setVertices(shader)
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4) #back  