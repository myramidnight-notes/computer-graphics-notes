from typing import TYPE_CHECKING
from OpenGL.GL import *
import OpenGL.GLU 
import numpy

if TYPE_CHECKING:
    from matrices import Point,Vector
    from shaders import Shader3D,Material

class MeshModel:
    def __init__(self):
        self.vertex_arrays :dict = {}
        # self.index_arrays = dict()
        self.mesh_materials :dict = {}
        self.materials :dict = {}
        self.vertex_counts :dict = {}
        self.vertex_buffer_ids :dict = {}

    def add_vertex(self, mesh_id:int, position:'Point', normal:'Vector', uv = None):
        if mesh_id not in self.vertex_arrays:
            self.vertex_arrays[mesh_id] = []
            self.vertex_counts[mesh_id] = 0
        self.vertex_arrays[mesh_id] += [position.x, position.y, position.z, normal.x, normal.y, normal.z]
        self.vertex_counts[mesh_id] += 1

    def set_mesh_material(self, mesh_id:int, mat_id:int):
        self.mesh_materials[mesh_id] = mat_id

    def add_material(self, mat_id:int, mat:'Material'):
        self.materials[mat_id] = mat
    
    def set_opengl_buffers(self):
        for mesh_id in self.mesh_materials.keys():
            self.vertex_buffer_ids[mesh_id] = glGenBuffers(1)
            glBindBuffer(GL_ARRAY_BUFFER, self.vertex_buffer_ids[mesh_id])
            glBufferData(GL_ARRAY_BUFFER, numpy.array(self.vertex_arrays[mesh_id], dtype='float32'), GL_STATIC_DRAW)
            glBindBuffer(GL_ARRAY_BUFFER, 0)

    def draw(self, shader:'Shader3D'):
        for mesh_id, mesh_material in self.mesh_materials.items():
            material = self.materials[mesh_material]
            shader.set_material(material)
            shader.set_attribute_buffers_with_normals(self.vertex_buffer_ids[mesh_id])
            glDrawArrays(GL_TRIANGLES, 0, self.vertex_counts[mesh_id])
            glBindBuffer(GL_ARRAY_BUFFER, 0)
