
from OpenGL.GL import *
from OpenGL.GLU import *
from math import *
from abc import ABC, abstractmethod

import pygame
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from shaders.ShaderBase import ShaderBase

class TextureStore(ABC):
    def __init__(self,shader:'ShaderBase'):
        """Takes in the shader so we don't have to pass it through when using textures"""
        self.shader = shader
        #self.starry_sky = self.load_texture("planet/starry_sky.jpg")

    def load_texture(self, texture_file):
        """Will load a texture from file path (within the assets/textures directory)
        \nit returns a texture"""
        texture_path = sys.path[0] + "\\assets\\textures\\" + texture_file.replace('/','\\')
        surface = pygame.image.load(texture_path)
        tex_string = pygame.image.tostring(surface, "RGBA", 1)  #convert image to a string
        width = surface.get_width()
        height = surface.get_height()
        tex_id = glGenTextures(1) 
        glBindTexture(GL_TEXTURE_2D, tex_id) #bind the texture so we can 
        # Specify texture units (defaults to GL_TEXTURE0 if you forget this, which is fine)
        glActiveTexture(GL_TEXTURE0)
        # Filters. These are for resizing, there are others for wrapping and such
        glTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR) 
        glTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
        # Finally we generate the image
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, tex_string)
        glBindTexture(GL_TEXTURE_2D,0) #unbind the texture since we're done
        return tex_id

    def diffuse(self, texture:int):
        """Sets the ActiveTexture and BindTexture for rendering
        \nFor the basic texture to apply to object"""        
        glActiveTexture(GL_TEXTURE0)
        glBindTexture(GL_TEXTURE_2D, texture) 
        self.shader.set_diffuse_texture()
        
    def specular(self, texture:int):
        """Sets the ActiveTexture and BindTexture for rendering
        \nJust this texture would be used for specular mapping"""        
        glActiveTexture(GL_TEXTURE1)
        glBindTexture(GL_TEXTURE_2D, texture) 
        self.shader.set_texture_specular()
    
    def resetDiffuse(self):
        """Unbind the diffuse texture"""
        glActiveTexture(GL_TEXTURE0)
        glBindTexture(GL_TEXTURE_2D,0)
        self.shader.reset_diffuse_texture()
        
    def resetSpecular(self):
        """Unbind the specular texture"""
        glActiveTexture(GL_TEXTURE1)
        glBindTexture(GL_TEXTURE_2D,0)
        self.shader.reset_specular_texture()
    
    def reset(self):
        """Resets all the textures through the shader"""
        self.resetDiffuse()
        self.resetSpecular()