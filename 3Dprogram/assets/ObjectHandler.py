from OpenGL.GL import *
import numpy
from shaders import Color, Material
from matrices import Point,Vector
import sys
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from shaders import Shader3D

# Obj loader and MeshModel is essentially straight from teacher,
# just adjusted a bit for my program

class MeshModel:
    def __init__(self):
        self.vertex_arrays = dict()
        # self.index_arrays = dict()
        self.mesh_materials = dict()
        self.materials = dict()
        self.vertex_counts = dict()
        self.vertex_buffer_ids = dict()

    def add_vertex(self, mesh_id, position, normal, uv = None):
        """Adds vertex to the list of the specified mesh"""
        #create a new entry in the dictionary if needed
        if mesh_id not in self.vertex_arrays:
            self.vertex_arrays[mesh_id] = []
            self.vertex_counts[mesh_id] = 0
        #add the vertex to that group
        self.vertex_arrays[mesh_id] += [position.x, position.y, position.z, normal.x, normal.y, normal.z]
        self.vertex_counts[mesh_id] += 1

    def set_mesh_material(self, mesh_id, mat_id):
        """Sets the material for given mesh, the mat_id is reference to the materials dictionary"""
        self.mesh_materials[mesh_id] = mat_id

    def add_material(self, mat_id, mat):
        """Adds a new material to the model"""
        self.materials[mat_id] = mat
    
    def set_opengl_buffers(self):
        """Creates a buffer for each of the mesh composing the model"""
        for mesh_id in self.mesh_materials.keys():
            self.vertex_buffer_ids[mesh_id] = glGenBuffers(1)
            glBindBuffer(GL_ARRAY_BUFFER, self.vertex_buffer_ids[mesh_id])
            glBufferData(GL_ARRAY_BUFFER, numpy.array(self.vertex_arrays[mesh_id], dtype='float32'), GL_STATIC_DRAW)
            glBindBuffer(GL_ARRAY_BUFFER, 0)

    def draw(self, shader:'Shader3D'):
        """Draws the model, it does not have any UV mapping currently"""
        for mesh_id, mesh_material in self.mesh_materials.items():
            material = self.materials[mesh_material]
            shader.set_material(material)
            shader.set_attribute_buffers_with_normals(self.vertex_buffer_ids[mesh_id])
            glDrawArrays(GL_TRIANGLES, 0, self.vertex_counts[mesh_id])
            glBindBuffer(GL_ARRAY_BUFFER, 0)

class ObjectHandler:
    def __init__(self, model_dir:str):
        self.filepath :str = sys.path[0].replace('\\','/') + "/" + model_dir 

    def load_mesh(self, filename:str):
        """Load a mesh model from a given obj file"""
        #extract the file location, in case the filename included sub directories
        currDir = self.filepath + filename
        findLastDir = currDir.rfind('/')
        curr_file_location = currDir[0:findLastDir+1]
        curr_file_name = currDir[findLastDir+1:]
        print("Filename: {}".format(curr_file_name))

        return self.__load_obj_file(curr_file_location, curr_file_name)
        
    def __load_mtl_file(self,file_location, file_name, mesh_model):
        """Loads the material library if available, used by the obj_loader,
        \nit comes as a complementary file with the obj file"""
        print("  Start loading MTL: " + file_name)
        mtl = None
        fin = open(file_location  + file_name)
        for line in fin.readlines():
            tokens = line.split()
            if len(tokens) == 0:
                continue
            if tokens[0] == "newmtl":
                print("    Material: " + tokens[1])
                mtl = Material()
                mesh_model.add_material(tokens[1], mtl)
            # Add diffuse to material
            elif tokens[0] == "Kd":
                mtl.diffuse = Color(float(tokens[1]), float(tokens[2]), float(tokens[3]))
            # Add specularity to material
            elif tokens[0] == "Ks":
                #mtl.specular = Color(0,0,0,1)
                mtl.specular = Color(float(tokens[1]), float(tokens[2]), float(tokens[3]))
            # Add shinyness to material
            elif tokens[0] == "Ns":
                mtl.shininess = float(tokens[1])
        print("  Finished loading MTL: " + file_name)

    def __load_obj_file(self,file_location, file_name):
        """Loads a model from a obj file"""
        print("Start loading OBJ: " + file_name)
        mesh_model = MeshModel()
        current_object_id = None
        current_position_list = []
        current_normal_list = []
        fin = open(file_location  + file_name)
        for line in fin.readlines():
            tokens = line.split()
            if len(tokens) == 0:
                continue
            if tokens[0] == "mtllib":
                self.__load_mtl_file(file_location, tokens[1], mesh_model)
            elif tokens[0] == "o":
                print("  Mesh: " + tokens[1])
                current_object_id = tokens[1]
                # current_position_list = []
                # current_normal_list = []
            # Collecting vertices
            elif tokens[0] == "v":
                current_position_list.append(Point(float(tokens[1]), float(tokens[2]), float(tokens[3])))
            # Collecting normals
            elif tokens[0] == "vn":
                current_normal_list.append(Vector(float(tokens[1]), float(tokens[2]), float(tokens[3])))
            # Load materials library (from a complementary file that should come with the obj file)
            elif tokens[0] == "usemtl":
                mesh_model.set_mesh_material(current_object_id, tokens[1])
            # Collect defined faces, a single face shares a single material
            elif tokens[0] == "f":
                for i in range(1, len(tokens)):
                    tokens[i] = tokens[i].split("/")
                vertex_count = len(tokens) - 1
                for i in range(vertex_count - 2):
                    if current_position_list == None:
                        current_position_list = []
                    if current_normal_list == None:
                        current_normal_list = []
                    # print(str(1) + " " + str(i+2) + " " + str(i+3))
                    # print(int(tokens[i+3][0]))
                    # print(int(tokens[i+3][2]))
                    # print("size: " + str(len(current_position_list)))
                    # print("size: " + str(len(current_normal_list)))
                    # print(int(tokens[1][0])-1)
                    # print(int(tokens[1][2])-1)
                    mesh_model.add_vertex(current_object_id, current_position_list[int(tokens[1][0])-1], current_normal_list[int(tokens[1][2])-1])
                    mesh_model.add_vertex(current_object_id, current_position_list[int(tokens[i+2][0])-1], current_normal_list[int(tokens[i+2][2])-1])
                    mesh_model.add_vertex(current_object_id, current_position_list[int(tokens[i+3][0])-1], current_normal_list[int(tokens[i+3][2])-1])
        mesh_model.set_opengl_buffers()
        print("Finished loading OBJ: " + file_name)
        return mesh_model
