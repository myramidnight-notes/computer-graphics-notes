import pygame
from pygame.locals import *
from typing import List


default_allowed_keys = sorted([
    K_ESCAPE, 
    K_a, K_s, K_d, K_w,     # movement
    K_t, K_g,               # zoom
    K_c,                    # camera toggle
    K_LEFT, K_RIGHT, K_UP, K_DOWN, # camera angle
])

class KeyInputHandler():
    def __init__(self, allowed_keys:List[int] = default_allowed_keys):
        """You can optionally set the allowed keys (using the pygame key values)
        \nhttp://www.pygame.org/docs/ref/key.html
        \nit makes it easy to disable keys without messing with the code"""
        self.__allowed = allowed_keys
        # Movement
        self.K_ESCAPE = False
        self.K_w = False 
        self.K_d = False 
        self.K_s = False 
        self.K_a = False 
        # Camera Tilt
        self.K_LEFT = False
        self.K_RIGHT = False
        self.K_UP = False
        self.K_DOWN = False
        # Zoom (changes field-of-view)
        self.K_t = False 
        self.K_g = False 
        # Rolling
        self.K_q = False
        self.K_e = False
        # Toggle camera
        self.K_c = False
    
    def key_input(self,event:pygame.event):
        """Takes in a pygame.event and checks for key input"""
        if event.type == pygame.KEYDOWN:
            if event.key in self.__allowed:
                if event.key == K_ESCAPE:
                    self.K_ESCAPE = True
                if event.key == K_c:
                    self.K_c = True
                if event.key == K_w:
                    self.K_w = True
                if event.key == K_a:
                    self.K_a = True
                if event.key == K_s:
                    self.K_s = True
                if event.key == K_d:
                    self.K_d = True
                if event.key == K_t:
                    self.K_t = True
                if event.key == K_g:
                    self.K_g = True
                if event.key == K_q:
                    self.K_q = True
                if event.key == K_e:
                    self.K_e = True
                if event.key == K_UP:
                    self.K_UP = True
                if event.key == K_DOWN:
                    self.K_DOWN = True
                if event.key == K_LEFT:
                    self.K_LEFT = True
                if event.key == K_RIGHT:
                    self.K_RIGHT = True

        elif event.type == pygame.KEYUP:
            if event.key in self.__allowed:
                if event.key == K_c:
                    self.K_c = False
                if event.key == K_w:
                    self.K_w = False
                if event.key == K_a:
                    self.K_a = False
                if event.key == K_s:
                    self.K_s = False
                if event.key == K_d:
                    self.K_d = False
                if event.key == K_t:
                    self.K_t = False
                if event.key == K_g:
                    self.K_g = False
                if event.key == K_q:
                    self.K_q = False
                if event.key == K_e:
                    self.K_e = False
                if event.key == K_UP:
                    self.K_UP = False
                if event.key == K_DOWN:
                    self.K_DOWN = False
                if event.key == K_LEFT:
                    self.K_LEFT = False
                if event.key == K_RIGHT:
                    self.K_RIGHT = False