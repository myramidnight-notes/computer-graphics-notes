from shaders import Color,Material
from typing import TYPE_CHECKING
from abc import ABC, abstractmethod
from OpenGL.GL import *

if TYPE_CHECKING:
    from shaders.ShaderBase import ShaderBase


class MaterialHandler(ABC):
    """COLORS!
    
    Easy access to preset colors, and then to pass them to the shader without
    having to specify the shader interaction within the program
    
    """
    def __init__(self, shader:'ShaderBase'): 
        self._shader:'ShaderBase' = shader  
        self.colors = {
            "white" : Color(1,1,1,1),
            "blue" : Color(0,0,1,1),
            "purple" : Color(0.4,0.2,0.5,1),
            "green": Color(0.4, 0.8, 0,1),
            "pink": Color(0.8, 0.2, 0.1,1),
            "lime": Color(0.8,1,0,1),
            "red" :Color(0.9,0,0.1,1),
            "orange": Color(0.9,0.5,0,1)
        }     
        self.white = Material(self.colors['white'])
        self.blue = Material(self.colors['blue'])
        self.green = Material(self.colors['green'])
        self.purple = Material(self.colors['purple'])
        self.pink = Material(self.colors['pink'])
        self.red = Material(self.colors['red'])
        self.orange = Material(self.colors['orange'])

    @abstractmethod
    def use(self, material:Material):
        self._shader.set_material(material)

    def _reset_texture_layer(self, layer:int):
        """Just specifies what texture layer to unbind (GL_TEXTURE_2D)"""
        #We don't want to be trying to unbind layers that don't exist
        if layer < GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS:
            glActiveTexture(GL_TEXTURE0+layer)
            glBindTexture(GL_TEXTURE_2D, 0)
        else:
            print("Texture layer out of range")

    @abstractmethod
    def reset(self):
        """Resets the material to the default white"""
        self._shader.set_material(Material(self.colors['white'], Color(0,0,0),1))

    def get_material(self, color:Color, specularity:Color = Color(0,0,0), shiny:float = 1):
        """Creates a material"""
        return Material(color, specularity, shiny)