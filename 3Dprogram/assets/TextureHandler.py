from OpenGL.GL import *
import pygame
import sys
import os
from typing import TYPE_CHECKING
from abc import ABC, abstractmethod

if TYPE_CHECKING:
    from shaders import Material, Shader3D, ShaderBase

class Texture:
    def __init__(self, data:str, height:float, width:float, name:str = 'untitled'):
        self.data = data
        self.width = width
        self.height = height
        self.name = name

class TextureHandler(ABC):
    def __init__(self, textures_dir:str):
        self.__texture_dir = sys.path[0].replace('\\','/') + "/" + textures_dir
        
    def get_texture(self, texture_file):
        """This actually returns a Texture class, containing the data string, height and width of image
        \nit has not bound the texture to a id"""
        texture_path = self.__texture_dir + texture_file
        if os.path.isfile(texture_path):
            surface = pygame.image.load(texture_path)
            tex_string = pygame.image.tostring(surface, "RGBA", 1)  #convert image to a string
            width = surface.get_width()
            height = surface.get_height()
            return Texture(tex_string,height,width,texture_file)
        else:
            print("Failed to decode the image")
            return None

    def load_texture(self, texture_file):
        """Will load a texture from file path (within the assets/textures directory)
        \nit returns a texture ID"""
        texture = self.get_texture(texture_file)
        tex_id = glGenTextures(1) 
        glBindTexture(GL_TEXTURE_2D, tex_id) #bind the texture so we can 
        # Specify texture units (defaults to GL_TEXTURE0 if you forget this, which is fine)
        glActiveTexture(GL_TEXTURE0)
        # Filters. These are for resizing, there are others for wrapping and such
        glTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR) 
        glTexParameter(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
        # Finally we generate the image
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texture.width, texture.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture.data)
        glBindTexture(GL_TEXTURE_2D,0) #unbind the texture since we're done
        return tex_id


    def load_cubemap(self, right:str, left:str, top:str, bottom:str, front:str, back:str):
        """Feed it the 6 images of the sides, and it shall spit out a texture ID to a cubemap
        
        TRYING to load cubemap, took a break from it. one source had GL_RGBA8 instead of GL_RGBA
        thought that would help. At the end I found this video, tried that, still didn't work
        https://www.youtube.com/watch?v=ZwNDBOUmyLY
        """
        faces = ['right','left','top','bottom','front','back']
        
        # textures = {
        #     'right': self.get_texture(right),
        #     'left':self.get_texture(left),
        #     'top':self.get_texture(top),
        #     'bottom':self.get_texture(bottom),
        #     'front':self.get_texture(front),
        #     'back':self.get_texture(back)
        # }

        # tex_id = glGenTextures(1) 
        # # Specify texture units (defaults to GL_TEXTURE0 if you forget this, which is fine)
        # glActiveTexture(GL_TEXTURE0)        
        # glBindTexture(GL_TEXTURE_CUBE_MAP, tex_id) #bind the texture so we can 
        # surface = pygame.image.load(self.__texture_dir + right).convert_alpha()
        # data = pygame.image.tostring(surface, "RGBA")  #convert image to a string
        # width = surface.get_width()
        # height = surface.get_height()

        # glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE)
        # glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE)
        # glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE)
        # glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        # glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR)

        # glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data)
        # glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data)
        # glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data)
        # glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data)
        # glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data)
        # glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data)
        

        # glBindTexture(GL_TEXTURE0,0) #unbind the texture since we're done


        return 0
