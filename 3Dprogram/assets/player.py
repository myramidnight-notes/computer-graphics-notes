from matrices import Point,Vector,ViewMatrix

class Player:
    """If we want 1st person player movement, good to have a 'body'"""
    def __init__(self, height:float = 1):
        self.height = height
        self.pos = Point(0,0,0)
        self.camera