# Blender
Because it's free and awesome

* Be careful of using UNDO, to figure out what it effects and what it doesn't, this messes me up.

## Everything is gray! 
> Materials not appearing on the model? try checking out the view mode
>![](images/blender_view_options.png)
>
>Those 4 circles there in the top right, they change how you see your model on screen.

