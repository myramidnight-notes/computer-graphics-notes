import pygame
from pygame.locals import *
from OpenGL.GL import *
from OpenGL.GLU import *
import random
from random import *

window_height = 600
window_width = 800
using_arrows = False
current_key = K_LEFT
boxes = []
selected_color = [0.0, 1.0, 1.0]

class Box:
    def __init__(self, x_pos, y_pos):
        self.size = 50
        self.speed = 0.03
        self.x_pos = x_pos
        self.y_pos = y_pos
        self.moving_left = False
        self.moving_up = False
        self.out_of_bounds = False
        self.selected = False
        self.color = [random(),random(),random()]

    def select_box(self, status:bool):
        if self.selected != status:
            self.selected = status


    def boundry(self):
        """Detect if the box has hit the edge of the window"""
        half_size = self.size/2
        edge_right = (self.x_pos + half_size) >= window_width
        edge_left = (self.x_pos - half_size) <= 0
        edge_bottom = (self.y_pos + half_size) >= window_height
        edge_top =(self.y_pos - half_size) <= 0

        # for the keyboard input, to stop the 
        if edge_right or edge_left or edge_top or edge_bottom:
            self.out_of_bounds = True
        else:
            self.out_of_bounds = False

        # set the directions
        if edge_left:
            self.moving_left = False
        elif edge_right:
            self.moving_left = True
        elif edge_top:
            self.moving_up = False
        elif edge_bottom:
            self.moving_up = True

    def move_box(self):
        """How the box moves on the screen"""
        self.boundry()
        # the up and down movement
        if self.selected and using_arrows and self.out_of_bounds == False: 
            # the sideways movement
            if current_key == K_RIGHT:
                if self.moving_left:
                    self.x_pos += self.speed
                else:
                    self.x_pos += self.speed * 2
            elif current_key == K_LEFT:
                if self.moving_left:
                    self.x_pos -= self.speed * 2
                else:
                    self.x_pos -= self.speed
            elif current_key == K_UP:
                if self.moving_up:
                    self.y_pos += self.speed
                else: 
                    self.y_pos += self.speed * 2
            elif current_key == K_DOWN:
                if self.moving_up:
                    self.y_pos -= self.speed * 2
                else:
                    self.y_pos -= self.speed
        # moving without keyboard input
        else:
            if self.moving_up:
                self.y_pos -= self.speed
            else:
                self.y_pos += self.speed

            if self.moving_left:
                self.x_pos -= self.speed
            else:
                self.x_pos += self.speed

    def rez_box(self):
        """Draw the box onto the screen"""
        # x1, y1, x2, y2
        half_box = self.size/2

        # color the selected/latest box a specific color
        if self.selected:
            glColor3f(selected_color[0],selected_color[1], selected_color[2])
        else:
            glColor3f(self.color[0],self.color[1], self.color[2])

        glRectf(self.x_pos - half_box, self.y_pos - half_box, self.x_pos + half_box, self.y_pos + half_box)

def init_game():
    pygame.display.init()
    pygame.display.set_mode((window_width,window_height), DOUBLEBUF|OPENGL)
    glClearColor(0.398, 0.199, 0.597, 1)

    #initiate the two first boxes
    autoBox = Box(50,50)
    controlBox = Box((window_height/2), (window_height/2))
    controlBox.select_box(True)
    boxes.append(autoBox)
    boxes.append(controlBox)

def update():
    for box in boxes:
        box.move_box()

def display():
    glClear(GL_COLOR_BUFFER_BIT)

    # display details
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    glViewport(0, 0, window_width, window_height)
    gluOrtho2D(0, window_width, 0, window_height)

    # drawing the generated boxes by mouse
    for box in boxes:
        box.rez_box()

    pygame.display.flip()


def loop():
    global boxes
    global using_arrows
    global current_key
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            quit()
        elif event.type == pygame.KEYDOWN:
            if event.key == K_ESCAPE:
                pygame.quit()
                quit()
            elif event.key == K_RIGHT or event.key == K_LEFT or event.key == K_UP or event.key == K_DOWN:
                using_arrows = True
                current_key = event.key

        elif event.type == pygame.KEYUP:
            if event.key == K_LEFT or event.key == K_RIGHT or event.key == K_UP or event.key == K_DOWN:
                using_arrows = False

        elif event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:
                x_pos = pygame.mouse.get_pos()[0]
                y_pos = pygame.mouse.get_pos()[1]
                # Add new box, and for some reason the y position provided by
                # the mouse seemed to invert the y axis for me, hence the abs
                boxes.append(Box(x_pos, abs(window_height - y_pos)))
    update()
    display()

if __name__ == "__main__":
    init_game()
    while True:
        loop()