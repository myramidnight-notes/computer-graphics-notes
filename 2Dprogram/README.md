# 2D graphics program
This folder holds 3 programs actually. 
* `pygame_basic` that is just a little example of getting things to appear on the screen
* `pygame_move` that focuses on getting things to move around
* `2Dgame/main` is the final product that builds on the previous two. A little interactive game.