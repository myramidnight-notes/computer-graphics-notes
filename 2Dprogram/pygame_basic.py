import pygame
from pygame.locals import *
from OpenGL.GL import *
from OpenGL.GLU import *
import random
from random import *

going_left = False
x_pos = 100
y_pos = 100


def init_game():
    pygame.display.init()
    pygame.display.set_mode((800, 600), DOUBLEBUF|OPENGL)
    glClearColor(1.0, 0.0, 0.0, 0.5)

def update():
    global x_pos
    global y_pos
    global going_left

    if going_left:    
        x_pos -= 0.01

def rez_tri():
    # draw a triangle
    glBegin(GL_TRIANGLES)
    # add this line if nothing appears or want to change the color
    glColor3f(0.0, 1.0, 1.0)
    glVertex2f(x_pos, 100)
    glVertex2f(x_pos, 200)
    glVertex2f(x_pos + 100, y_pos)
    glEnd()

def rez_box(x1, y1, x2, y2):
    glRectf(x1,y1, x2, y2)

def display():
    # add the color from glClearColor
    glClear(GL_COLOR_BUFFER_BIT)

    # display details
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    glViewport(0, 0, 800, 600)
    gluOrtho2D(0, 800, 0, 600)
    
    rez_tri()
    rez_box(100,250,150,100)
    # the flip should be at the end of display()
    pygame.display.flip()


def game_loop():
    global going_left

    # way to exit the game
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            quit()
        elif event.type == pygame.KEYDOWN:
            if event.key == K_ESCAPE:
                pygame.quit()
                quit()
            elif event.key == K_q:
                # to randomize the background color when pressing Q
                glClearColor(random(), random(), random(), 1.0)
            elif event.key == K_LEFT:
                going_left = True

        elif event.type == pygame.KEYUP:
            if event.key == K_LEFT:
                going_left = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:
                x_pos = pygame.mouse.get_pos()[0]
                y_pos = pygame.mouse.get_pos()[1]
    update()
    display()

if __name__ == "__main__":
    init_game()
    while True:
        game_loop()