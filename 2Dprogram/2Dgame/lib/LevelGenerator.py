from typing import List
from lib.assets.Diamond import Diamond
from lib.assets.Brick import Brick
from lib.assets.Level import Level

from pygame.math import Vector2


def generateFull(columns:int,rows:int,width:float,height:float):
    #only want the top 3rd of the screen
    col_width = width/columns
    row_height = (height/3)/columns

    curr_x = col_width/2
    curr_y = height - 20

    bricks = []

    for row in range(rows):
        curr_x = col_width/2
        curr_y -= row_height
        #Generate the bricks
        for brick in range(columns):
            bricks.append(Brick(
                pos = Vector2(curr_x,curr_y),
                score = 10 * (row+1),
                height = row_height,
                width = col_width))
            curr_x += col_width

    return bricks


def LevelGenerator(view_height, view_width) -> List:
    """Creates preset levels (contained within)"""
    return [
        # Level 1
        Level(
            #Generate breakable bricks, columns, rows
            breakables= generateFull(1,1,view_width,view_height),
            #Static obsticles
            obsticles=[
                Diamond(Vector2((view_width/4),250),3),
                Diamond(Vector2((view_width/4)*3,250),3),
                ]
        ),
        # Level 2
        Level(
            #Generate breakable bricks, columns, rows
            breakables= 
                generateFull(2,1,view_width,view_height),
            #Static obsticles
            obsticles=[
                Diamond(Vector2((view_width/4),250),3),
                Diamond(Vector2((view_width/4)*3,250),3)
                ]
        ),
        # Level 3
        Level(
            breakables= 
                generateFull(5,5,view_width,view_height),
            obsticles=[
                Diamond(Vector2((view_width/4),300),3),
                Diamond(Vector2((view_width/4)*3,250),3)
                ]
        ),
    ]