import math
from pygame.math import Vector2

def t_hit(b_direction:Vector2, a_direction:Vector2, point_a:Vector2, point_b:Vector2):
    hit = (b_direction.dot(point_b - point_b))/b_direction.dot(a_direction)
    return hit

def p_hit(point_a:Vector2, t_hit:float, a_direction:Vector2):
    point = point_a + (a_direction.scale_to_length(t_hit))
    return point

def reflect(original:Vector2, norm: Vector2):
    return original.reflect(norm)

def get_normal(point_a:Vector2, point_b:Vector2):
    """Returns the normals of a line (in either direction) 
    \n[ Vector(-x,y) , Vector(x,-y) ]"""
    dx = point_b.x - point_a.x
    dy = point_b.y - point_a.y

    return [Vector2(dy*(-1), dx), Vector2(dy, dx*(-1))]

def get_box_normals(top_left:Vector2,top_right:Vector2,bot_right:Vector2,bot_left:Vector2):
    """Get all the normals of the sides, takes vectors(top_left, top_right, bot_right, bot_left), 
    \ninput the limits and get the vectors"""
    # adjusted so that the box can be at any rotation, 
    # calculated from the corners
    l_vector = get_normal(bot_left,top_left)
    r_vector = get_normal(bot_right, top_right)
    t_vector = get_normal(top_left, top_right)
    b_vector = get_normal(bot_left, bot_right)
    
    return [l_vector,r_vector,t_vector,b_vector]
       

def get_outer_normals(top_left:Vector2,top_right:Vector2,bot_right:Vector2,bot_left:Vector2):
    """Takes the corners of a box in order, returns a list of outer facing normals
    \nReturns: [left,right,top,bottom]"""
    normals = get_box_normals(top_left,top_right,bot_right,bot_left)
    return [normals[0][1], normals[1][0], normals[2][0], normals[3][1]]   

def get_inner_normals(top_left:Vector2,top_right:Vector2,bot_right:Vector2,bot_left:Vector2):
    """Returns the normal vectors for the inner sides of the box,
    \nReturns a list of vectors [left,right,top,bottom]"""
    normals = get_box_normals(top_left,top_right,bot_right,bot_left)
    return [normals[0][0], normals[1][1], normals[2][1], normals[3][0]]   

def rotate_point(point:Vector2, pivot:Vector2, angle:float):
    """Will return a Vector2 that has been roated around the pivot point
    at an angle (in radians)"""

    origin = Vector2(point.x - pivot.x, point.y-pivot.x)

    new_x = origin.x *  - origin.y * math.sin(angle)
    new_y = origin.x * math.sin(angle) + origin.y * math.cos(angle)

    #rotated and re adjusted
    return Vector2(new_x + point.x, new_y + point.y)

def distance_to(point_a:Vector2, point_b:Vector2, point: Vector2) -> float:
    """Calculates distance from a point to a line, 
    \n point_a and point_b define the line
    \nhttps://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line"""

    dx1 = point_b.x - point_a.x
    dy1 = point_a.y - point.y
    dx2 = point_a.x - point.x
    dy2 = point_b.y - point_a.y

    eqtop = abs((dx1*dy1) - (dx2*dy2))

    dx3 = (point_b.x - point_a.x) ** 2
    dy3 = (point_b.y - point_a.y) ** 2

    return eqtop/(math.sqrt(dx3+dy3))


