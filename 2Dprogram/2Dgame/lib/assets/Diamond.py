from OpenGL.GL import *
from OpenGL.GLU import *
import pygame
from pygame.math import Vector2
from lib.methods import distance_to, get_outer_normals
from lib.assets.Block import Block

class Diamond(Block):
    """Creates a diamond shaped block"""
    def __init__(self,pos:Vector2,scalar:float, breakable = False,):

        self._corners = []        
        #set the default size in parent class
        super().__init__(
            pos = pos,
            scalar = scalar,
            width = 50,
            height = 20,
            breakable = breakable,
            score = 30
        )        


    def _check_distance(self,point_a, point_b,point):
        """Checks the distance of point to the line"""
        margin = 3
        dist = distance_to(point_a,point_b,point)
        return (dist<=margin)

    def set_hitbox(self) -> None:
        """Sets the hitbox and corners"""
        #calculate from position and size where the hitbox sits
        hitbox = pygame.Rect(
            self._pos.x - ((self._width/2)*self._scalar),    
            self._pos.y - ((self._height/2)*self._scalar),
            self._width*self._scalar,
            self._height*self._scalar
        )

        #vectors for corners of diamond
        self._corners = [
            Vector2(hitbox.center[0],hitbox.top),       #top
            Vector2(hitbox.right,hitbox.center[1]),     #right
            Vector2(hitbox.center[0],hitbox.bottom),    #bottom
            Vector2(hitbox.left,hitbox.center[1])       #left
        ]

        self._hitbox = hitbox
        self._side_normal = get_outer_normals(self._corners[0],self._corners[1],self._corners[2],self._corners[3])
    
    def hit_check(self,point:Vector2):
        """A function to detect what edge to bounce off"""
        in_hitbox = self._hitbox.collidepoint(point.x,point.y)
        
        if in_hitbox:
            #top left
            if self._check_distance(self._corners[0],self._corners[3],point):
                return self._side_normal[0]
            #top right
            elif self._check_distance(self._corners[0],self._corners[1],point):
                return self._side_normal[2]
            #bottom right
            elif self._check_distance(self._corners[2],self._corners[1],point):
                return self._side_normal[1]
            #bottom left
            elif self._check_distance(self._corners[2],self._corners[3],point):
                return self._side_normal[3]
                
        return None
        
    def draw(self) -> None:
        #position is in center of box
        half_width = self._width/2
        half_height = self._height/2
        glPushMatrix()

        glTranslate(self._pos.x, self._pos.y, 0)
        glScalef(self._scalar,self._scalar,0)

        glBegin(GL_TRIANGLE_FAN)
        glVertex2f(0,0)
        glVertex2f(-half_width,0)
        glVertex2f(0,half_height)
        glVertex2f(half_width,0)
        glVertex2f(0,-half_height)
        glVertex2f(-half_width,0)
        glEnd()
        glPopMatrix()

        
