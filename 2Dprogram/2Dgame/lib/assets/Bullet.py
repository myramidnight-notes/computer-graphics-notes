import math
from OpenGL.GL import *
from OpenGL.GLU import *
from pygame.math import Vector2

class Bullet:
    def __init__(self):
        self.radius = 80 #where the bullet comes out
        self._direction: Vector2 = None
        self._pos = None
        self.color = [0,0,0]

        self.reset_bullet(0)

    def set_angle(self,angle) -> Vector2:
        '''Angle in degrees'''
        return Vector2(math.cos(angle), math.sin(angle))

    def get_pos(self) -> Vector2:
        """For getting the current position of the bullet"""
        return self._pos

    def get_direction(self) -> Vector2:
        """Returns the directional vector of the bullet"""
        return self._direction

    def bounce(self,surface:Vector2) -> None:
        """Takes in the normal vector of the contact surface, 
        and reflects the bullet to a new direction"""
        self._direction = self._direction.reflect(surface)
        
    def reset_bullet(self,angle) -> None:
        '''Reusable bullet, we just reset the start point'''
        # bullet is rezzed at a radius from cannon center
        x = 400 + (self.radius * math.cos(angle))
        y = (self.radius * math.sin(angle))
        self._pos = Vector2(x,y)
        self._direction = Vector2(math.cos(angle), math.sin(angle))
        
    def draw(self) -> None:
        glPushMatrix()
        glTranslate(self._pos.x, self._pos.y, 0)
        glRotate(self._direction.angle_to(self._pos),0,0,1)
        glScale(2,2,0)
        glBegin(GL_TRIANGLE_FAN)
        
        glColor3f(0.0, 1.0, 1.0)
        glVertex2f(0, 0)
        glVertex2f(-1, -3)
        glVertex2f(-3, -1)
        glVertex2f(-3, 1)
        glVertex2f(-1,3)
        glVertex2f(1,3)
        glVertex2f(3,1)
        glVertex2f(3,-1)
        glVertex2f(1,-3)
        glVertex2f(-1,-3)
        glEnd()

        glPopMatrix()
    

    def update(self, speed):
        dx = self._direction.x * speed
        dy = self._direction.y * speed
        self._pos.x = self._pos.x + dx
        self._pos.y = self._pos.y + dy