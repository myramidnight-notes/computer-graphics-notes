from pygame.math import Vector2
from lib.assets.TextDisplay import TextDisplay
from OpenGL.GL import *
import pygame
from random import random

class Overlay:
    def __init__(self, width:float, height:float, color = (1,1,1,1)):
        """Creates a full screen overlay with centered text"""
        self.__rect = pygame.Rect(0,height,width,height)
        self.__color = [random(),random(),random()]
        self.__text = TextDisplay(Vector2(width/2, width/2))
        self.__visible = False
    
    def set_visible(self,visible:bool):
        self.__visible = visible

    def draw(self, text):
        glPushMatrix()

        if self.__visible:
            glColor3f(self.__color[0],self.__color[1], self.__color[2])
            glRectf(self.__rect.left,self.__rect.bottom, self.__rect.right, self.__rect.top)
            self.__text.draw(text)
        
        glPopMatrix()
