

from lib.assets.Block import Block
from typing import List


class Level:
    """
    Constructs levels with obsticles and breakable bricks.
    """
    def __init__(self, breakables: List[Block], obsticles: List[Block] = []):
        self.__obsticles = obsticles
        self.__breakable =  breakables

    def update(self, bullet):
        score = 0
        for box in self.__obsticles:
            hit = box.hit_check(bullet.get_pos())
            if hit != None:
                bullet.bounce(hit)

        
        for brick in self.__breakable:
            hit = brick.hit_check(bullet.get_pos())
            if hit != None:
                #collect the score
                score += hit[0]
                bullet.bounce(hit[1])
                #remove the broken brick
                self.__breakable.remove(brick)
            
        return score
        
        


    def draw(self):
        
        #obsticles
        for block in self.__obsticles:
            block.draw()

        #breakables
        for block in self.__breakable:
            block.draw()

    def total_left(self):
        """Returns a number of bricks left to break"""
        return len(self.__breakable)