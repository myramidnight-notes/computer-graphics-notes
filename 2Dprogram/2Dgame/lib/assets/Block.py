from abc import ABC, abstractmethod
from pygame.math import Vector2

class Block(ABC):
    """Parent class to the various shapes on screen"""
    def __init__(self, pos:Vector2, scalar:float = 1, height = 20, width = 20,breakable = False, score:float = 10):
        self._pos = pos
        self._height = height
        self._width = width
        self._scalar = scalar
        self._hitbox = None
        self._side_normal = []
        self._breakable = breakable
        self._score = score
        self.set_hitbox()

    @abstractmethod
    def set_hitbox(self):
        """A method that initiates the pygame.Rect for hitbox"""
        pass

    @abstractmethod
    def hit_check(self,point:Vector2):
        """Handles what the brick will do when hit, if it hits"""
        pass

    @abstractmethod
    def draw():
        pass