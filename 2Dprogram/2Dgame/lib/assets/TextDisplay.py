from OpenGL.GL import *
import pygame

class TextDisplay:
    def __init__(self, pos: pygame.Vector2, color=(255,255,255)):
        self.__pos = pos
        self.__size = 30
        self.__color = color

    def draw_format(self,text:str,*args:str):
        """If we want to print a formatted text with arguments"""
        self.draw(text.format(*args))

    def set_color(self,color:tuple):
        """Takes in a color in RGBA form: (255,255,255) to update font color"""
        self.__color = color

    def draw(self,text:str):
        """Draws the provided text onto the screen, with transparent background"""
        font = pygame.font.Font (None, self.__size)
        textSurface = font.render(text, True, self.__color).convert_alpha()    
        textRect = textSurface.get_rect() #get the rect for positioning

        # Set the alignment
        textRect.bottomright= (self.__pos.x, self.__pos.y) #align the text's bottom right pos
        
        textData = pygame.image.tostring(textSurface, "RGBA", True)     
        glRasterPos2d(self.__pos.x, self.__pos.y)     

        #Make background transparent
        glEnable(GL_BLEND)
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

        #Draw the actual text
        glDrawPixels(textSurface.get_width(), textSurface.get_height(), GL_RGBA, GL_UNSIGNED_BYTE, textData)
        glDisable(GL_BLEND)

        