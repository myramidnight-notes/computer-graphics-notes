from lib.assets.Block import Block
from typing import List
from lib.methods import get_outer_normals
from OpenGL.GL import *
from OpenGL.GLU import *
import pygame
from pygame.math import Vector2
from random import random

class Brick(Block):
    """A breakable block"""
    def __init__(self, pos:Vector2, score:int, scalar:float = 1, height:float = 10, width:float=10):
        self._color = [random(),random(),random()]
        #set the default size in parent class
        super().__init__(
            pos = pos,
            scalar = scalar, 
            height = height,
            width = width,
            score = score
        )

    def set_hitbox(self) -> None:
        """Sets the hitbox for the brick for easier collision detection 
        \nand anything else needed for handling collisions"""
        #set the Rect from pygame to detect when point enters the area
        hitbox = pygame.Rect(self._pos.x ,self._pos.y ,self._width*self._scalar,self._height*self._scalar)
        hitbox.center = (self._pos.x, self._pos.y)
        #set the edge normals so we can bounce the bullet
        self._side_normal = get_outer_normals(
            Vector2(hitbox.left, hitbox.top),
            Vector2(hitbox.right, hitbox.top),
            Vector2(hitbox.right, hitbox.bottom),
            Vector2(hitbox.left, hitbox.bottom)
        )

        self._hitbox = hitbox

    def hit_check(self,point:Vector2) -> List:
        """For updating the brick, what happens when the bullet hits it
        \nreturns [score:int, side_vector:Vector2]"""
        if self._hitbox.collidepoint(point.x,point.y):
            return [self._score, self.check_side(point)]
        return None

    def check_side(self, point:Vector2) -> Vector2:
        """Checking which side was touched"""
        margin = 2 # margin to make sure we catch the hit

        # check the sides
        if point.x - self._hitbox.left <= margin:   # left
            return self._side_normal[1]
        elif point.x - self._hitbox.right <= margin:  # right
            return self._side_normal[2]
        elif point.y - self._hitbox.top <= margin:    # top
            return self._side_normal[0] 
        elif point.y - self._hitbox.bottom <= margin: # bottom
            return self._side_normal[3] 


    def draw(self) -> None:
        """Draw the brick"""
        glPushMatrix()
        # === for some reason my bricks turn invisible due to this, while hitbox is there
        #glTranslate(self._pos.x, self._pos.y, 0)
        #glScalef(self._scalar,self._scalar,0)
        glColor3f(self._color[0],self._color[1], self._color[2])
        glRectf(self._hitbox.left,self._hitbox.bottom, self._hitbox.right, self._hitbox.top)
        
        glPopMatrix()