
import math
from OpenGL.GL import *
from OpenGL.GLU import *
from pygame.math import Vector2

class Cannon:
    def __init__(self, view_width):
        self._radius = 80
        self._width = view_width
        self._center = Vector2(view_width/2,0)
        self._vector = Vector2(view_width/2,self._radius)
        self._angle:float = math.radians(90)

    def calc_angle(self, center) -> None:
        '''Calculates the angle basted on vectors
        \nwas for the purpose of calculating angles based on mouse input originally
        '''
        x = self._vector.x - center.x
        y = self._vector.y - center.y
        angle =  math.atan2(y,x)
        return angle

    def update_angle(self):
        new_angle = self.calc_angle(self._center)
        self._angle = new_angle

    def get_angle(self)->Vector2:
        """Returns the directional vector of the bullet"""
        return self._angle

    def draw(self) -> None:
        """Draws the cannon on screen"""
        #TODO get the sprite to rotate visibly (bullet proves that angle is updated)
        glPushMatrix()
        glTranslate(400,0,0) #moves cannon to center
        glRotatef(self.calc_angle(Vector2(0,0)),1,0,0)
        glScale(2,2,0)

        glBegin(GL_TRIANGLE_STRIP)
        glColor3f(0.0, 1.0, 1.0)
        glVertex2f(5, 0)
        glVertex2f(-5, 0)
        glVertex2f(15, 10)
        glVertex2f(-15,10)
        glVertex2f(5,40)
        glVertex2f(-5,40)
        glEnd()

        glPopMatrix()

    def move_cannon(self,direction, speed):
        '''For moving cannon with keyboard'''
        if direction == 'left':
            if self._vector.x > 0:
                speed = speed * -1
            else:
                return
        elif direction == 'right':
            if self._vector.x >= self._width:
                return

        self._vector.x = self._vector.x + speed
        self._vector.y = self._radius
        self.update_angle()

