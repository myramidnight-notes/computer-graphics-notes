
from lib.assets.Overlay import Overlay
import pygame
from pygame.locals import *
from OpenGL.GL import *
from OpenGL.GLU import *
from pygame.math import Vector2

from lib.methods import *

# Assets
from lib.assets.Cannon import Cannon
from lib.assets.Bullet import Bullet
from lib.assets.TextDisplay import TextDisplay
from lib.LevelGenerator import LevelGenerator

class Game:
    # ============= INIT =================
    def __init__(self):
        '''Start your engines (pygame)'''
        # Attributes
        self._view_height = 600
        self._view_width = 800
        self._angle = 90
        self._total_score = 0
        self._winner = False

        # Game time
        self._clock = pygame.time.Clock()
        self._clock.tick()

        # The cannon
        self._cannon = Cannon(self._view_width)
        self._moving = False
        self._direction = 'left'

        #Reusable bullet
        self._bullet = Bullet()
        self._bullet_moving = False
        self._mouse_shot = False

        #Levels
        self.__levels = LevelGenerator(self._view_height, self._view_width)

        self.__scoreboard = TextDisplay(Vector2(10,10))
        self.__overlay = Overlay(self._view_width,self._view_height)

        self.__level_counter = 0
        self.__curr_level = self.__levels[0]

        #Speeds
        self._bullet_speed = 5
        self._cannon_speed = 2

        # init pygame
        pygame.init()
        pygame.display.set_mode((self._view_width,self._view_height), DOUBLEBUF|OPENGL)
        glClearColor(0.398, 0.199, 0.597, 1)
        self._boundry = self._set_boundry()
       

    def _set_boundry(self):
        """Sets the collision properties for the edges of the viewport"""
        top_left = Vector2(0,self._view_height )
        top_right = Vector2(self._view_width,self._view_height)
        bot_right = Vector2(self._view_width,0)
        bot_left = Vector2(0,0)
        return get_inner_normals(top_left,top_right,bot_right,bot_left)

    def _check_win(self):
        #TODO make a win condition that works, implement
        pass

    # ===================================
    #  UPDATE          
    # ===================================

    def _update_cannon(self,delta_time):
        """Handles updating the cannon properties"""
        if self._moving and not self._bullet_moving:
            #being moved with keyboard
            self._cannon.move_cannon(self._direction, self._cannon_speed * delta_time)


    def _update_bullet(self,delta_time):
        """Handles updating the bullet"""
        if self._bullet_moving:
            #keep the bullet moving
            self._bullet.update(self._bullet_speed * delta_time)

            bullet_pos = self._bullet.get_pos()
            
            #Check if the bullet hits any of the viewport edges
            if bullet_pos.x <= 0:                   #hit the left side
                self._bullet.bounce(self._boundry[0])
            elif bullet_pos.x > self._view_width:   #hit the right side
                self._bullet.bounce(self._boundry[1])
            elif bullet_pos.y > self._view_height:  #hit the top
                self._bullet.bounce(self._boundry[2])
            elif bullet_pos.y <= 0:     #it fell off the bottom edge
                self._bullet_moving = False
        else:
            self._bullet.reset_bullet(self._cannon.get_angle())
            self._bullet.update(0)

    def _update(self):
        '''Update things for next frame'''

        #game time in seconds
        delta_time = self._clock.tick() / 10

        if self._winner or (self.__curr_level.total_left() > 0):
            #Is the cannon being moved
            self._update_cannon(delta_time)
                
            #update level (obsticles and breakables)

            #Is there already a bullet bouncing around?
            self._update_bullet(delta_time)
            score = self.__curr_level.update(self._bullet)
            if score > 0:
                self._total_score += score
        else:
            self._winner = True
            self.__overlay.set_visible(True)

        
    # ===================================
    # DISPLAY 
    # ===================================
    def display(self):
        '''Display the current frame'''
        # add the color from glClearColor
        glClear(GL_COLOR_BUFFER_BIT)

        # display details
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        glViewport(0, 0, self._view_width, self._view_height)
        gluOrtho2D(0, self._view_width, 0, self._view_height)
        # draw stuff 
        self._cannon.draw()
        # Level
        self.__curr_level.draw()

        self._bullet.draw()

        self.__scoreboard.draw("Score: {}".format(str(self._total_score)))

        if self._winner:
            if len(self.__levels) - self.__level_counter - 1 > 0:
                self.__overlay.draw("Winner! Press any key to continue")
            else:
                self.__overlay.draw("Game over! No more levels")
        
        # the flip should be at the end of display()
        pygame.display.flip()


    # ===================================
    # GAME COMPONENTS 
    # ===================================

    def shoot(self, mouse):
        """This handles the bullet initiation, so that only one bullet is
        on the screen at a time"""
        if self._bullet_moving:
            return  #Cannot shoot if bullet is already on map
        else:
            direction = self._cannon.get_angle()
            self._bullet.reset_bullet(direction)
            self._bullet_moving = True

    
    # ===================================
    #  LOOP / INPUT HANDLER
    # ===================================
    def next_level(self):
        """Continues game to next level, resetting bullet and other things"""
        if len(self.__levels) - self.__level_counter - 1 > 0:
            self.__level_counter += 1
            self.__curr_level = self.__levels[self.__level_counter]
            self._winner = False
            self._bullet_moving = False
            self._bullet.reset_bullet(self._cannon.get_angle())
            self.__overlay.set_visible(False)

    def game_loop(self):
        '''input handler'''

        # way to exit the game
        for event in pygame.event.get():
            # When closing the window
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
                
            # When pressing keys
            elif event.type == pygame.KEYDOWN:
                if event.key == K_ESCAPE:
                    pygame.quit()
                    quit()

                if self._winner:
                    self.next_level()
                else:
                    # ESC to quit game
                    if event.key == K_SPACE:
                        self.shoot(False)
                    elif event.key == K_RIGHT:
                        self._moving = True
                        self._direction = 'right'
                    elif event.key == K_LEFT:
                        self._moving = True
                        self._direction = 'left'

            elif event.type == pygame.KEYUP:
                if event.key == K_RIGHT or event.key == K_LEFT:
                    self._moving = False

            #elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            #        self._mouse_shot = True
            #        self.shoot(True)
        self._update()
        self.display()

# ===================================
# MAIN 
# ===================================
if __name__ == "__main__":
    game = Game()
    while True:
        game.game_loop()